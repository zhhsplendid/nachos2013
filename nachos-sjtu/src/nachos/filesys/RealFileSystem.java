package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.vm.SwapFile;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
  /** the free list */
  private FreeList free_list;
  
  /** the root folder */
  private Folder root_folder;
  
  /** the current folder */
  private Folder cur_folder;
  
  /** the string representation of the current folder */
  private LinkedList<String> cur_path = new LinkedList<String>();
  
  private static char dbgFileSystem = 'f';
  
  /**
   * initialize the file system
   * 
   * @param format
   *          whether to format the file system
   */
  public void init (boolean format)
  {
    if (format)
    {
    	DiskManager.initlize();
    	INode inode_free_list = INode.inodeForCreateFile(FreeList.STATIC_ADDR, INode.TYPE_SYSTEM);
    	free_list = new FreeList(inode_free_list, this);
    	free_list.init();
    	
    	INode inode_root_folder = INode.inodeForCreateFile(Folder.STATIC_ADDR, INode.TYPE_FOLDER);
    	root_folder = new Folder(inode_root_folder, "root", null, this);
    	
    	cur_folder = root_folder;
    	importStub();
    }
    else
    {
      DiskManager.initlize();
      INode inode_free_list = new INode(FreeList.STATIC_ADDR);
      inode_free_list.load();
      free_list = new FreeList(inode_free_list, this);
      free_list.load();
      
      INode inode_root_folder = new INode(Folder.STATIC_ADDR);
      inode_root_folder.load();
      root_folder = new Folder(inode_root_folder, "root", null, this);
      root_folder.load();
      
      cur_folder = root_folder;
    }
  }
  
  public void finish ()
  {
    root_folder.save();
    free_list.save();
  }
  
  /** import from stub filesystem */
  private void importStub ()
  {
    FileSystem stubFS = Machine.stubFileSystem();
    FileSystem realFS = FilesysKernel.realFileSystem;
    String[] file_list = Machine.stubFileList();
    for (int i = 0; i < file_list.length; ++i)
    {
      if (!file_list[i].endsWith(".coff"))
        continue;
      OpenFile src = stubFS.open(file_list[i], false);
      if (src == null)
      {
        continue;
      }
      OpenFile dst = realFS.open(file_list[i], true);
      int size = src.length();
      byte[] buffer = new byte[size];
      src.read(0, buffer, 0, size);
      dst.write(0, buffer, 0, size);
      Lib.debug(dbgFileSystem, "read " + src.getName() + " ...write " + dst.getName());
      
      //Lib.debug(dbgFileSystem, src.length() + "-bytes " + dst.length() + "-bytes");
      src.close();
      dst.close();
    }
  }
  
  /** get the only free list of the file system */
  public FreeList getFreeList ()
  {
    return free_list;
  }
  
  /** get the only root folder of the file system */
  public Folder getRootFolder ()
  {
    return root_folder;
  }
  
  /**
   * return a file; if create is false, just return file or null(doesn't exist)
   * if create is true, return file existed or create it(doen't exist)
   * 
   * @return OpenFile
   */
  public OpenFile open (String dir, boolean create)
  {
	int fileINode;
	Folder folder;
	String[] path_name = getPathName(dir);
	if(path_name == null){
		return null;
	}
	String path = path_name[0];
	String name = path_name[1];
	
	if(path == null){
		//cur_folder.load();
		folder = cur_folder;
	}
	else{
		File file = getPathFile(path);
		if(file == null || !(file instanceof Folder)){
			return null;
		}
		folder = (Folder)file;
	}
	
	INode inode;
	if(create){
		fileINode = folder.create(name);
	}
	else{
		fileINode = folder.open(name);
	}
	
	if(fileINode == -1){
		return null;
	} 
	
	inode = INode.loadINode(fileINode);
	
	if(inode.file_type != INode.TYPE_FILE && inode.file_type != INode.TYPE_FILE_DEL 
			&& inode.file_type != INode.TYPE_SYMLINK){
		Lib.debug(dbgFileSystem, "can't open a file type like that");
		return null;
	}
	File returnfile;
	if(inode.file_type == INode.TYPE_SYMLINK){
		returnfile = handleSymLink(inode.addr, name, folder);
		if(returnfile == null){
			return null;
		}
	}
	else{
		returnfile = new File(inode, name, cur_folder, this);
	}
    
    ++returnfile.inode.use_count;
    returnfile.inode.saveUseCount();
    
    return returnfile;
  }
  
  /**
	 * Atomically remove an existing file. After a file is removed, it cannot be
	 * opened until it is created again with <tt>open</tt>. If the file is
	 * already open, it is up to the implementation to decide whether the file
	 * can still be accessed or if it is deleted immediately. (in nachos.machine.FileSystem.java)
	 * 
	 * I decide to let the file can still be access --by Splendid Zheng 2013.11.16
	 */
  public boolean remove (String dir)
  { 
	Folder folder;
	String[] path_name = getPathName(dir);
	if(path_name == null){
		return false;
	}
	String path = path_name[0];
	String name = path_name[1];
		
	if(path == null){
		//cur_folder.load();
		folder = cur_folder;
	}
	else{
		File file = getPathFile(path);
		if(file == null || !(file instanceof Folder)){
			return false;
		}
		folder = (Folder)file;
	}
		
	int inode_addr = folder.getINodeAddr(name);
	if(inode_addr == -1){
		Lib.debug(dbgFileSystem, "there doesn't exist such file to remove");
		return false;
	}
	
	INode inode = INode.loadINode(inode_addr);
	if(inode.file_type == INode.TYPE_FOLDER || inode.file_type == INode.TYPE_FOLDER_DEL){ 
		//return false;
		
		Lib.debug(dbgFileSystem, "warning: remove a whole folder though this filesystem supports^_^");
		folder.removeEntry(name);
		Folder toRemove = Folder.loadFolder(inode, name, folder, this);
		toRemove.removeAll(true);
		
		return true;
		
	}
	
	if(inode.file_type == INode.TYPE_SYMLINK){
		folder.removeEntry(name);
		inode.free();
		return true;
	}
	inode.file_type = INode.TYPE_FILE_DEL;
	--inode.link_count;
	folder.removeEntry(name);
	
	if(inode.use_count == 0 && inode.link_count == 0){	
		inode.free();
		return true;
	}
	inode.save();
    return true;
  }
  
  /**
   * create a folder in directory;
   * 
   * @param dir
   * @return 0 for success of -1 for failure
   */
  public int createFolder (String dir)
  {
	String[] path_name = getPathName(dir);
	if(path_name == null){
		return -1;
	}
	String path = path_name[0];
	String name = path_name[1];
	
	if(path == null){
		//cur_folder.load();
		return cur_folder.createFolder(name);
	}
	else{
		File file = getPathFile(path);
		if(file == null || !(file instanceof Folder)){
			return -1;
		}
		Folder folder = (Folder)file;
		int success = folder.createFolder(name);
		return success;
	}
  }
  
  /**
   * only a empty folder can be removed
   * 
   * @param dir
   * @return
   */
  public int removeEmptyFolder(String dir){
	String[] path_name = getPathName(dir);
	if(path_name == null){
		return -1;
	}
	String path = path_name[0];
	String name = path_name[1];
		
	if(path == null){
		//cur_folder.load();
		int success = cur_folder.removeEmptyFolder(name);
		return success;
	}
	else{
		File file = getPathFile(path);
		if(file == null || !(file instanceof Folder)){
			return -1;
		}
		Folder folder = (Folder)file;
		int success = folder.removeEmptyFolder(name);
		return success;
	}
  }
  
  /**
   * remove all file in this folder
   * @param name
   * @return
   */
  public boolean removeFolder (Folder beRemove)
  {	
	beRemove.removeAll(true);
	
    return true;
  }
  
  /**
   * change the cur_folder to the folder given by name, just same like
   * linux cd ...
   * 
   * @param name
   * @return
   */
  public int changeCurFolder (String name)
  {
	File file = getPathFile(name);
	if(file == null || !(file instanceof Folder)){
		return -1;
	}
	Folder folder = (Folder)file;
	cur_folder.close();
	cur_folder = folder;
	++cur_folder.inode.use_count;
	cur_folder.inode.save();
    return 0;
  }
  
  /**
   * read all entry's name in this folder, the same as linux's ls command
   * @param pathName
   * @return
   */
  public String[] readDir (String pathName)
  {
	File file = getPathFile(pathName);
	if(file == null){
		return null;
	}
	else if(file instanceof Folder){
		return ((Folder)file).listString();
	}
	else{
		String[] filename = new String[1];
		filename[0] = file.getName();
		return filename;
	}
  }
  
  /**
   * return a file state including imformations about file
   * @param name
   * @return
   */
  public FileStat getStat (String name)
  {
	File file = getPathFile(name);
	if(file == null){
		return null;
	}
	INode inode = file.inode;
	return new FileStat(name, inode.file_size, inode.file_type, inode.addr, inode.link_count);
  }
  
  /**
   * create a link from src to dst
   * @param src
   * @param dst
   * @return
   */
  public int createLink (String src, String dst)
  {
	File dstfile = getPathFile(dst);
	if(dstfile == null){
		return -1;
	}
	String[] path_name = getPathName(src);
	if(path_name == null){
		return -1;
	}
	String path = path_name[0];
	String name = path_name[1];
	
	Folder folder;
	if(path == null){
		//cur_folder.load();
		folder = cur_folder;
	}
	else{
		File file = getPathFile(path);
		if(file == null || !(file instanceof Folder)){
			return -1;
		}
		folder = (Folder)file;
	}
	
	if(folder.getINodeAddr(name) != -1){
		return -1;
	}
	int addr = dstfile.inode.addr;
	folder.addEntry(name, addr);
	folder.save();
	++dstfile.inode.link_count;
	dstfile.inode.saveLinkCount();
    return 0;
  }
  
  /**
   * create symlink from src to dst
   * @param src
   * @param dst
   * @return
   */
  public int createSymlink (String src, String dst)
  {
	File dstfile = getPathFile(dst);
	if(dstfile == null){
		return -1;
	}
	String dir = getDirToFile(dstfile);
	
	String[] path_name = getPathName(src);
	if(path_name == null){
		return -1;
	}
	String path = path_name[0];
	String name = path_name[1];
	
	Folder folder;
	if(path == null){
		//cur_folder.load();
		folder = cur_folder;
	}
	else{
		File file = getPathFile(path);
		if(file == null || !(file instanceof Folder)){
			return -1;
		}
		folder = (Folder)file;
	}
	
	if(folder.getINodeAddr(name) != -1){
		return -1;
	}
	int addr = free_list.allocate();
	INode inode = INode.inodeForCreateFile(addr, INode.TYPE_SYMLINK);
	SymLink symlink = SymLink.createSymLink(inode, dir, name, folder, this);
	symlink.save();
	folder.addEntry(name, addr);
	folder.save();
    return 0;
  }
  
  /**
   * print the absolute path to cur_folder
   * 
   * @return
   */
  public String getCurWorkDir(){
	  return getDirToFile(cur_folder);
  }
  
  public int getSwapFileSectors(){
	  int size = SwapFile.getInstance().getOpenSwapFile().length();
	  return INode.getSectorCount(size);
  }
  
  public int getFreeSize(){
	  return free_list.getFreeSize();
  }
  /***************************************************
   * tools                                           *
   ***************************************************/
  
  public String getDirToFile(File file){
	  String dir = "";
	  if(file == null){
		  return null;
	  }
	  while(file.father_folder != null){
		  dir = "/" + file.getName() + dir;
		  file = file.father_folder;
	  }
	  if(dir.equals("")){
		  dir = "/";
	  }
	  return dir;
  }
  
  
  private boolean isAbsolutePath(String path){
	  return path.charAt(0) == '/';
  }
  
  private Folder getPathFolder(String dir){
	  String[] path_name = getPathName(dir);
	  String path = path_name[0];
	  //String name = path_name[1];
	  
	  if(path == null){
		  //cur_folder.load();
		  return cur_folder;
	  }
	  else{
		  File file = getPathFile(path);
		  if(file == null || !(file instanceof Folder)){
			  return null;
		  }
		  return (Folder)file;
	  }
	  
  }
  
  private File getPathFile(String path){
	  if(path == null || path.length() < 1){
		  return null;
	  }
	  File file;
	  if(isAbsolutePath(path)){
		  file = handleAbsolutePath(path);
	  }
	  else{
		  file = handleRelativePath(path);
	  }
	  if(file != null && file.inode.addr == cur_folder.inode.addr){
		  file = cur_folder;
	  }
	  return file;
  }
  private File handleAbsolutePath(String path){
	  int cur = Folder.STATIC_ADDR;
	  Folder now = root_folder;
	  return pathFrom(cur, now, path);
  }
  
  private File handleRelativePath(String path){
	  int cur = cur_folder.inode.addr;
	  //cur_folder.load();
	  Folder now = cur_folder;
	  path = "/" + path;
	  
	  return pathFrom(cur, now, path);
  }
  
  /**
   * from relative path to cur and now, return the file which path refer to
   * 
   * @param cur
   * @param now
   * @param path
   * @return
   */
  private File pathFrom(int cur, Folder now, String path){
	  String sub = now.getName();
	  String last = null;
	  while(path.length() > 1){
		  int l = path.indexOf('/');
		  int r = path.indexOf('/', l + 1);
		  if(r == -1){
			  last = sub;
			  sub = path.substring(l + 1);
			  path = "";
		  }
		  else{
			  last = sub;
			  sub = path.substring(l + 1, r);
			  path = path.substring(r);
		  }
		  
		  if(sub.equals("..")){
			  if(now == root_folder) continue;
			  else{
				  now = now.father_folder;
				  cur = now.inode.addr;
				  continue;
			  }
		  }
		  else if(sub.equals(".")){
			  sub = last;
			  continue;
			  /* in fact bug: here in linux, should assert "." now file
			   * is a folder, but my nachos filesystem ignore that. because...lazy and quickly
			   *
			   * if someone wants to change it, just check inode of cur's type must be a folder
			   * or symlink to folder
			   */
			 
		  }
		  else if(cur == Folder.STATIC_ADDR){
			  now = root_folder;
		  }
		  else{
			  INode inode = INode.loadINode(cur);
			  if(inode.file_type == INode.TYPE_SYMLINK){
				  while(inode.file_type == INode.TYPE_SYMLINK){
					File tmp = handleSymLink(cur, sub, now);
				  	cur = tmp.inode.addr;
				  	inode = INode.loadINode(cur);
				  	if(tmp instanceof Folder){
					  now = (Folder)tmp;
					  break;
				  	}
				  }
			  }
			  else if(inode.file_type == INode.TYPE_FOLDER || inode.file_type == INode.TYPE_FOLDER_DEL){
				  now = Folder.loadFolder(inode, last, now, this);
			  }
			  else{
				  return null;
			  }
		  }
		  
		  cur = now.getINodeAddr(sub);
		  if(cur == -1){
			  return null;
		  }
	  }
	  
	  INode inode = INode.loadINode(cur);
	  if(cur == Folder.STATIC_ADDR){
		  return root_folder;
	  }
	  if(inode.file_type == INode.TYPE_FILE || inode.file_type == INode.TYPE_FILE_DEL){
		  return new File(inode, sub, now, this);
	  }
	  if(inode.file_type == INode.TYPE_FOLDER || inode.file_type == INode.TYPE_FOLDER_DEL){
		  return Folder.loadFolder(inode, sub, now, this);
	  }
	  if(inode.file_type == INode.TYPE_SYMLINK){
		  return SymLink.loadSymLink(inode, sub, now, this);
	  }
	  return null;
  }
  
  /**
   * return file refered by a symlink
   * 
   * @param inode_addr
   * @param name
   * @param father
   * @return
   */
  File handleSymLink(int inode_addr, String name, Folder father){
	INode inode = INode.loadINode(inode_addr);
	SymLink symlink = SymLink.loadSymLink(inode, name, father, this);
	String check = symlink.getPath();
	Lib.assertTrue(check.charAt(0) == '/');
	return handleAbsolutePath(check);
  }
  
  /**
   * input a path, spilt it into two parts: path to create and create name 
   * @return String[0] = path, String[1] = name;
   */
  private String[] getPathName(String str){
	  if(str == null || str.length() < 1){
		  return null;
	  }
	  String[] path_name = new String[2]; 
	  if(str.equals("/")){
		  return null;
	  }
	  
	  int r;
	  if(str.charAt(str.length() - 1) == '/'){
		  r = str.lastIndexOf('/', str.length() - 1);
	  }
	  else{
		  r = str.lastIndexOf('/');
	  }
	  
	  if(r == -1){
		  path_name[1] = str;
		  return path_name;
	  }
	  else{
		  path_name[1] = str.substring(r+1);
		  if(r != 0){
			  path_name[0] = str.substring(0, r);
		  }
		  else{
			  path_name[0] = "/";
		  }
		  return path_name;
	  }
  }
}
