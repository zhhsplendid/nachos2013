package nachos.filesys;

import nachos.machine.*;
import nachos.threads.ThreadedKernel;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
	INode inode;
	
	Folder father_folder;
	/** file read/write position measure by byte */
	private int pos;
	
	public int dbgFile = 'f';
	
	public File (INode inode, String name, Folder father, FileSystem file_system)
	{
		super(file_system, name);
		this.inode = inode;
		father_folder = father;
		pos = 0;
	}
  
	public int length ()
	{
		inode = INode.loadINode(inode.addr);
		return inode.file_size;
	}
	
	public void close ()
	{
		inode = INode.loadINode(inode.addr);
		--inode.use_count;
		if(inode.file_type == INode.TYPE_FILE_DEL || inode.file_type == INode.TYPE_FOLDER_DEL){
			if(inode.use_count == 0 && inode.link_count == 0){
				inode.free();
			}
		}
		inode.saveUseCount();
	}
  
	public void seek (int pos)
	{
		this.pos = pos;
	}
  
	public int tell ()
	{
		return pos;
	}
  
	public int read (byte[] buffer, int offset, int length)
	{
		int ret = read(pos, buffer, offset, length);
		pos += ret;
		return ret;
	}
  
	public int write (byte[] buffer, int offset, int length)
	{
		int ret = write(pos, buffer, offset, length);
		pos += ret;
		return ret;
	}
  
	/**
	 * function to read from disk
	 * 
	 * @param start: the start index read into buffer
	 * @param limit: read until file end or buffer reach limit
	 * @return how many bytes have been read
	 */
	public int read (int pos, byte[] buffer, int offset, int length)
  	{
		delay();
		int readbytes = 0;
		int amount = 0;
		inode = INode.loadINode(inode.addr);
		length = Math.min(length, inode.file_size - pos);
		while(pos < inode.file_size && length > 0){
			int sectorNumber = inode.getSector(pos);
			Lib.assertTrue(sectorNumber != -1);
			
			int sectorOffset = INode.getOffset(pos);
			amount = DiskManager.readSector(sectorNumber, sectorOffset, buffer, offset, length);
			readbytes += amount;
			
			pos += amount;
			offset += amount;
			length -= amount;
		}
		return readbytes;
		/*
		if(pos >= inode.file_size){
			return 0;
		}
		int sectorNumber = inode.getSector(pos);
		Lib.assertTrue(sectorNumber != -1);
		
		int sectorOffset = INode.getOffset(pos);
	  	
		length = Math.min(length, inode.file_size - pos);
		int amount = DiskManager.readSector(sectorNumber, sectorOffset, buffer, offset, length);
		
	  	if(amount < length){
	  		return amount + read(pos + amount, buffer, offset + amount, length - amount);
	  	}
	  	else return amount;
	  	*/
  	}
	
	public int write (int pos, byte[] buffer, int offset, int length){
		delay();
		inode = INode.loadINode(inode.addr);
		int amount = writeWithoutSetSize(pos, buffer, offset, length);
		if(amount + pos > inode.file_size){
			inode.setFileSize(amount + pos);
		}
		return amount;
	}
	
  	public int writeWithoutSetSize(int pos, byte[] buffer, int offset, int length)
  	{
		int sectorNumber = inode.getSector(pos);
		if(sectorNumber == -1){
			inode.getFreeSector(pos + length);
			sectorNumber = inode.getSector(pos);
			if(sectorNumber == -1){
				return 0;
			}
		}
		
		int sectorOffset = INode.getOffset(pos);
		
		int amount = DiskManager.writeSector(sectorNumber, sectorOffset, buffer, offset, length);
	  		
	  	if(amount < length){
	  		return amount + writeWithoutSetSize(pos + amount, 
	  				buffer, offset + amount, length - amount);
	  	}
	  	else return amount;
  	}
  	
  	private void delay() {
		//long time = Machine.timer().getTime();
		int amount = 1;
		ThreadedKernel.alarm.waitUntil(amount);
		//Lib.assertTrue(Machine.timer().getTime() >= time + amount);
	}
}
