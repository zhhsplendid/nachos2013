package nachos.filesys;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
  protected static final int SYSCALL_MKDIR = 14;
  protected static final int SYSCALL_RMDIR = 15;
  protected static final int SYSCALL_CHDIR = 16;
  protected static final int SYSCALL_GETCWD = 17;
  protected static final int SYSCALL_READDIR = 18;
  protected static final int SYSCALL_STAT = 19;
  protected static final int SYSCALL_LINK = 20;
  protected static final int SYSCALL_SYMLINK = 21;
  
  private final char dbgFileSys = 'f'; 
  
  public int handleSyscall (int syscall, int a0, int a1, int a2, int a3)
  {
    switch (syscall)
    {
      case SYSCALL_MKDIR:
      {
    	  return handleMKDIR(a0);
      }
        
      case SYSCALL_RMDIR:
      {
    	  return handleRMDIR(a0);
      }
        
      case SYSCALL_CHDIR:
      {
    	  return handleCHDIR(a0);
      }
        
      case SYSCALL_GETCWD:
      {
    	  return handleGETCWD(a0, a1);
      }
        
      case SYSCALL_READDIR:
      {
    	  return handleReadDir(a0, a1, a2, a3);
      }
        
      case SYSCALL_STAT:
      {
    	  return handleSTAT(a0, a1);
      }
       
      case SYSCALL_LINK:
      {
    	  return handleLink(a0, a1);
      }
      
      case SYSCALL_SYMLINK:
      {
    	  return handleSymLink(a0, a1);
      }
      
      default:
        return super.handleSyscall(syscall, a0, a1, a2, a3);
    }
  }
  
  public void handleException (int cause)
  {
    if (cause == Processor.exceptionSyscall)
    {
      Processor processor = Machine.processor();
      int result = handleSyscall(processor.readRegister(Processor.regV0),
				processor.readRegister(Processor.regA0), processor
						.readRegister(Processor.regA1), processor
						.readRegister(Processor.regA2), processor
						.readRegister(Processor.regA3));
	  processor.writeRegister(Processor.regV0, result);
      processor.advancePC();
    }
    else
      super.handleException(cause);
  }
  
  private int handleMKDIR(int pathNamePoint){
	  String dir = readVirtualMemoryString(pathNamePoint, maxStringLength);
	  return FilesysKernel.realFileSystem.createFolder(dir);
  }
  
  private int handleRMDIR(int pathNamePoint){
	  String dir = readVirtualMemoryString(pathNamePoint, maxStringLength);
	  return FilesysKernel.realFileSystem.removeEmptyFolder(dir);
  }
  
  private int handleCHDIR(int pathNamePoint){
	  String dir = readVirtualMemoryString(pathNamePoint, maxStringLength);
	  return FilesysKernel.realFileSystem.changeCurFolder(dir);
  }
  
  private int handleGETCWD(int bufferPoint, int size){
	  String dir = FilesysKernel.realFileSystem.getCurWorkDir();
	  return writeVirtualMemoryString(bufferPoint, dir, size);
  }
  
  private int handleReadDir(int dirNamePoint, int bufferPoint, int size, int namesize){
	  String dir = readVirtualMemoryString(dirNamePoint, maxStringLength);
	  String[] ls = FilesysKernel.realFileSystem.readDir(dir);
	  for(int i = 0; i < ls.length; ++i){
		  if(i >= size){
			  return -1;
		  }
		  int amount = writeVirtualMemoryString(bufferPoint + i * namesize, ls[i], namesize);
		  if(amount == -1){
			  return -1;
		  }
	  }
	  return ls.length;
  }
  
  private int handleSTAT(int fileNamePoint, int statPoint){
	  String dir = readVirtualMemoryString(fileNamePoint, maxStringLength);
	  FileStat fileStat = FilesysKernel.realFileSystem.getStat(dir);
	  //TODO
	  byte[] data = new byte[4 * FileStat.intValueCount];
	  writeVirtualMemoryString(statPoint, fileStat.name, FileStat.FILE_NAME_MAX_LEN);
	  Lib.bytesFromInt(data, 0, fileStat.size);
	  Lib.bytesFromInt(data, 4, fileStat.sectors);
	  Lib.bytesFromInt(data, 8, fileStat.type);
	  Lib.bytesFromInt(data, 12, fileStat.inode);
	  Lib.bytesFromInt(data, 16, fileStat.links);
	  writeVirtualMemory(statPoint + FileStat.FILE_NAME_MAX_LEN, data);
	  return 0;
  }
  
  private int handleLink(int oldNamePoint, int newNamePoint){
	  String dst = readVirtualMemoryString(oldNamePoint, maxStringLength);
	  String src = readVirtualMemoryString(newNamePoint, maxStringLength);
	  return FilesysKernel.realFileSystem.createLink(src, dst);
  }
  
  private int handleSymLink(int oldNamePoint, int newNamePoint){
	  String dst = readVirtualMemoryString(oldNamePoint, maxStringLength);
	  String src = readVirtualMemoryString(newNamePoint, maxStringLength);
	  return FilesysKernel.realFileSystem.createSymlink (src, dst);
  }
  /********************************************
   * tools                                    *
   ********************************************/
  
  public int writeVirtualMemoryString(int vaddr, String str, int MaxLength){
	  Lib.assertTrue(str != null);
	  byte[] data = str.getBytes();
	  if(data.length + 1 > MaxLength){
		  return -1;
	  }
	  else{
		  byte[] end = {0};
		  int amount = writeVirtualMemory(vaddr, data);
		  amount += writeVirtualMemory(vaddr + amount, end);
		  return amount;
	  }
  }
}
