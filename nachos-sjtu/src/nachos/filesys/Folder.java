package nachos.filesys;

import java.util.*;
import java.util.Map.Entry;

import nachos.machine.FileSystem;
import nachos.machine.Lib;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends File
{
  /** the static address for root folder */
  public static int STATIC_ADDR = 1;
  
  /** how many files in this folder; only used in save & load */
  private int size;
  
  /** mapping from filename to folder entry */
  private Hashtable<String, Integer> entry;
  
  //public boolean dirty;
  
  public Folder (INode inode, String name, Folder father, FileSystem file_system)
  {
    super(inode, name, father, file_system);
    entry = new Hashtable<String, Integer>();
    //dirty = false;
  }
  
  public static Folder loadFolder(INode inode, String name, Folder father, FileSystem file_system){
	  Folder folder = new Folder(inode, name, father, file_system);
	  folder.load();
	  return folder;
  }
  
  /** open a file in the folder and return its address */
  public int open (String filename)
  {
	Integer addr = entry.get(filename);
	if(addr == null){
		return -1;
	}
	return addr;
  }
  
  /** create a new file in the folder and return its address */
  public int create (String filename)
  {
    Integer addr = entry.get(filename);
    if(addr == null){
    	INode inode = INode.create();
    	addr = inode.addr;
    	addEntry(filename, addr);
    }
    return addr;
  }
  
  public int createFolder (String filename){
	  Integer addr = entry.get(filename);
	  if(addr != null){
		  return -1;
	  }
	  addr = FilesysKernel.realFileSystem.getFreeList().allocate();
	  INode.inodeForCreateFile(addr, INode.TYPE_FOLDER);
	  addEntry(filename, addr);
	  
	  save();
	  //dirty = true;
	  return 0;
  }
  
  public int removeEmptyFolder(String filename){
	  Integer addr = entry.get(filename);
	  if(addr == null){
		  return -1;
	  }
	  INode inode = INode.loadINode(addr);
	  if(inode.file_type != INode.TYPE_FOLDER && inode.file_type != INode.TYPE_FOLDER_DEL){
		  return -1;
	  }
	  if(inode.file_size > 4){ 
		  //it isn't a empty inode;
		  return -1;
	  }
	  
	  inode.file_type = INode.TYPE_FOLDER_DEL;
	  --inode.link_count;
	  removeEntry(filename);
	  
	  if(inode.link_count == 0 && inode.use_count == 0){
		  inode.free();
		  return 0;
	  }
	  inode.save();
	  return 0;
  }
  
  /**
   * get entry's inode addr of filename
   * @param filename
   * @return Inode addr 
   */
  public int getINodeAddr(String filename){
	  Integer addr = entry.get(filename);
	  if(addr == null){
		  return -1;
	  }
	  return addr;
  }
  
  /**
   * remove all file in this folder, incorrect and useless
   * to savety, comment it;
   */
  
  public void removeAll(boolean isFirst){
	  Iterator<Entry<String, Integer>> iter = entry.entrySet().iterator();
	  while(iter.hasNext()){
		Entry<String, Integer> iter_entry = iter.next();
		
		String name = iter_entry.getKey();
		int addr = iter_entry.getValue();
		
		INode inode = INode.loadINode(addr);
		
		if(inode.file_type == INode.TYPE_FOLDER || inode.file_type == INode.TYPE_FOLDER_DEL){
		  Folder folder = loadFolder(inode, name, this, null);
		  folder.removeAll(false);
		  iter.remove();
		}
		else if(inode.file_type == INode.TYPE_FILE || inode.file_type == INode.TYPE_FILE_DEL){
		  --inode.link_count;
		  inode.file_type = INode.TYPE_FILE_DEL;
		  iter.remove();
		  if(inode.link_count == 0 && inode.use_count == 0){
			inode.free();
			continue;
		  }
		  inode.save();
		}
		else if(inode.file_type == INode.TYPE_SYMLINK){
		  //TODO maybe wrong
		  --inode.link_count;
		  iter.remove();
		  if(inode.link_count == 0 && inode.use_count == 0){
			inode.free();
			continue;
		  }
		  inode.saveLinkCount();
		}
	  }
	  
	  this.inode.file_type = INode.TYPE_FOLDER_DEL;
	  --this.inode.link_count;
	  
	  if(this.inode.use_count == 0 && this.inode.link_count == 0){
		inode.free();
		return;
	  }
	  this.save();
	  inode.save();
  }
  
  /** add an entry with specific filename and address to the folder */
  public void addEntry (String filename, int addr)
  {
	  entry.put(filename, addr);
	  //dirty = true;
	  save();
  }
  
  /** remove an entry from the folder */
  public void removeEntry (String filename)
  {
	  if(entry.remove(filename) != null){
		  //dirty = true;
		  save();
	  }
  }
  
  public String[] listString(){
	  String[] ls = new String[entry.size()];
	  Iterator<Entry<String, Integer>> iter = entry.entrySet().iterator();
	  int i = 0;
	  while(iter.hasNext()){
		  Entry<String, Integer> iter_entry = iter.next();
		  ls[i++] = iter_entry.getKey();
	  }
	  return ls;
  }
  
  public boolean isEmpty(){
	  return entry.isEmpty();
  }
  /** save the content of the folder to the disk */
  public void save ()
  {
	  size = entry.size();
	  Iterator<Entry<String, Integer>> iter = entry.entrySet().iterator();
	  byte[] save_size = Lib.bytesFromInt(size);
	  write(0, save_size, 0, 4);
	  int pos = 4;
	  while(iter.hasNext()){
		  Entry<String, Integer> iter_entry = iter.next();
		  String filename = iter_entry.getKey();
		  int addr = iter_entry.getValue();
		  
		  byte[] save_addr = Lib.bytesFromInt(addr);
		  byte[] save_name = filename.getBytes();
		  byte[] name_length = Lib.bytesFromInt(save_name.length);
		  
		  pos += write(pos, save_addr, 0, 4);
		  pos += write(pos, name_length, 0, 4);
		  pos += write(pos, save_name, 0, save_name.length);
	  }
	  if(pos < inode.file_size){
		inode.setFileSize(pos);
		inode.freeSector(pos);  
	  }
  }
  
  /** load the content of the folder from the disk */
  public void load ()
  {
    byte[] load_size = new byte[4];
    read(0, load_size, 0, 4);
    size = Lib.bytesToInt(load_size, 0);
    int pos = 4;
    for(int i = 0; i < size; ++i){
    	byte[] data = new byte[8];
    	pos += read(pos, data, 0, 8);
    	int addr = Lib.bytesToInt(data, 0);
    	int name_length = Lib.bytesToInt(data, 4);
    	
    	byte[] name = new byte[name_length];
    	pos += read(pos, name, 0, name_length);
    	String filename = new String(name);
    	addEntry(filename, addr);
    }
  }
}
