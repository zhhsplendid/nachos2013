package nachos.filesys;

import nachos.machine.FileSystem;
import nachos.machine.Lib;

public class SymLink extends File{

	private String path;
	private byte[] data;
	
	public SymLink(INode inode, String name, Folder father, FileSystem file_system){
		super(inode, name, father, file_system);
	}
	
	public static SymLink createSymLink(INode inode, String dir, String name, Folder father, FileSystem file_system) {
		SymLink create = new SymLink(inode, name, father, file_system);
		create.setPath(dir);
		return create;
	}
	
	public static SymLink loadSymLink(INode inode, String name, Folder father, FileSystem file_system) {
		SymLink load_symlink = new SymLink(inode, name, father, file_system);
		load_symlink.load();
		return load_symlink;
	}

	public String getPath(){
		return path;
	}
	
	public void setPath(String str){
		path = str;
		data = path.getBytes();
	}
	
	public void save(){
		byte[] size = Lib.bytesFromInt(data.length);
		write(0, size, 0, 4);
		write(4, data, 0, data.length);
	}
	
	public void load(){
		byte[] size = new byte[4];
		read(0, size, 0, 4);
		int len = Lib.bytesToInt(size, 0);
		data = new byte[len];
		read(4, data, 0, len);
		path = new String(data);
	}
}
