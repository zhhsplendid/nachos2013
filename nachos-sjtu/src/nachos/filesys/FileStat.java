package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Lib;

public class FileStat
{
  public static final int FILE_NAME_MAX_LEN = 256;
  public static final int NORMAL_FILE_TYPE = 0;
  public static final int DIR_FILE_TYPE = 1;
  public static final int LinkFileType = 2;
  
  public static final int intValueCount = 5;
  
  public String name;
  public int size;
  public int sectors;
  public int type;
  public int inode;
  public int links;
  
  public FileStat(String filename, int filesize, int inode_type, int inode_addr, int linkcount){
	  name = filename;
	  size = filesize;
	  
	  sectors = INode.getSectorCount(filesize);
	 
	  if(inode_type == INode.TYPE_FILE || inode_type == INode.TYPE_FILE_DEL){
		  type = NORMAL_FILE_TYPE;
	  }
	  else if(inode_type == INode.TYPE_FOLDER || inode_type == INode.TYPE_FILE_DEL){
		  type = DIR_FILE_TYPE;
	  }
	  else if(inode_type == INode.TYPE_SYMLINK){
		  type = LinkFileType;
	  }
	  else{
		  Lib.assertNotReached("read type error");
	  }
	  
	  inode = inode_addr;
	  
	  links = linkcount;
  }
}
