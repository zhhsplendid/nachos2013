package nachos.filesys;

import java.util.*;

import nachos.machine.Disk;
import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.threads.Lock;

/**
 * FreeList is a single special file used to manage free space of the filesystem.
 * It maintains a list of sector numbers to indicate those that are available to use.
 * When there's a need to allocate a new sector in the filesystem, call allocate().
 * And you should call deallocate() to free space at a appropriate time (eg. when a file is deleted) for reuse in the future.
 * 
 * @author starforever
 */
public class FreeList extends File
{
  /** the static address */
  public static int STATIC_ADDR = 0;
  
  /** size occupied in the disk (bitmap) */
  static int size = Lib.divRoundUp(Disk.NumSectors, 8);
  
  /** maintain address of all the free sectors */
  private LinkedList<Integer> free_list;
  
  public static int SECTOR_BEGIN_FOR_USER = 4;
  
  private Lock lock = new Lock();
  
  public FreeList (INode inode, FileSystem file_system)//may change not need inode
  {
     super(inode, "freelist", null, file_system);
     inode.file_type = INode.TYPE_SYSTEM;
     free_list = new LinkedList<Integer>();
  }
  
  public void init ()
  {
      for (int i = SECTOR_BEGIN_FOR_USER; i < Disk.NumSectors; ++i){
    	  free_list.add(i);
      }
  }
  
  /** allocate a new sector in the disk */
  public int allocate ()
  {
	  lock.acquire();
	  if(free_list.isEmpty()){
		  return -1;
	  }
	  int freeSector = free_list.poll();
	  lock.release();
	  return freeSector;
  }
  
  /**
   * allocate need sectors in the disk, failed if return null
   * @param needNum
   * @return a array of sector numbers of null when failed
   */
  public int[] allocateSectors(int needNum){
	  Lib.assertTrue(needNum >= 0);
	  lock.acquire();
	  if(free_list.size() < needNum){
		  lock.release();
		  return null;
	  }
	  int[] sectors = new int[needNum];
	  for(int i = 0; i < needNum; ++i){
		  sectors[i] = free_list.poll();
	  }
	  lock.release();
	  return sectors;
  }
  
  /** deallocate a sector to be reused */
  public void deallocate (int sec)
  {
	  lock.acquire();
	  free_list.add(sec);
	  lock.release();
  }
  
  /**
   * return how many sectors are free in free_list
   * @return free sectors count 
   */
  public int getFreeSize(){
	  return free_list.size();
  } 
  
  /** save the content of freelist to the disk */
  public void save ()
  {
	  byte[] saveFreeList = new byte[size];
	  for(Integer sector: free_list){
		  saveFreeList[getBitMapPos(sector)] |= getBitMapOffset(sector); 
	  }
	  write(0, saveFreeList, 0, size);
  }
  
  /** load the content of freelist from the disk */
  public void load ()
  {
	  byte[] date = new byte[size];
	  read(0, date, 0, size);
	  for(int i = 0; i < size; ++i){
		  int withoutOffset = i * 8; 
		  int offset = (int)(date[i]);
		  for(int j = 0; j < 8; ++j){
			  if( ((offset >> j) & 1) == 1){
				  free_list.add(withoutOffset + j);
			  }
		  }
	  }
  }
  
  /*
   * follow there are some utilities for save & load 
   */
  
  
  private int getBitMapPos(int sector){
	return sector / 8;
  }
  
  private byte getBitMapOffset(int sector){
	return (byte)(1 << (sector % 8));	
  }
}
