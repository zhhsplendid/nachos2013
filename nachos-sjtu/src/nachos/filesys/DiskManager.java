package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.threads.Condition;
import nachos.threads.Lock;

import java.util.*;

public class DiskManager {
	
	private static final int cacheNumber = 32;
	
	private static byte[][] cache = new byte [cacheNumber][Disk.SectorSize];
	
	private static boolean[] dirty = new boolean[cacheNumber];
	
	private static int[] sectors = new int[cacheNumber];
	
	private static LinkedList<Integer> LRU_list = new LinkedList<Integer>();
	
	private static Lock lock = new Lock();
	//private static ConcurrencyController concurCtrler = new ConcurrencyController();
	
	public static void initlize(){
		for(int i = 0; i < cacheNumber; ++i){
			sectors[i] = -1;
		}
	}
	
	private static void writeToCache(int pos, int sectorOffset, byte[] data, int offset, int amount){
		System.arraycopy(data, offset, cache[pos], sectorOffset, amount);
		dirty[pos] = true;
		LRU_list.remove(new Integer(pos));
		LRU_list.add(pos);
	}
	
	private static void readFromCache(int pos, int sectorOffset, byte[] data, int offset, int amount){
		System.arraycopy(cache[pos], sectorOffset, data, offset, amount);
		LRU_list.remove(new Integer(pos));
		LRU_list.add(pos);
	}
	
	private static int pickReplaced(){
		return pickLRU();
		//return pickRandom();
	}
	
	private static int pickLRU(){
		for(int i = 0; i < cacheNumber; ++i){
			if(sectors[i] == -1){
				return i;
			}
		}
		return LRU_list.getFirst();
	}
	
	private static int pickRandom(){
		for(int i = 0; i < cacheNumber; ++i){
			if(sectors[i] == -1){
				return i;
			}
		}
		return Lib.random(cacheNumber);
	}
	
	private static int sectorInCache(int sectorNumber){
		for(int i = 0; i < cacheNumber; ++i){
			if(sectors[i] == sectorNumber){
				return i;
			}
		}
		return -1;
	}
	
	private static void replace(int pos, int sectorNumber){
		if(dirty[pos]){
			Machine.synchDisk().writeSector(sectors[pos], cache[pos], 0);
		}
		Machine.synchDisk().readSector(sectorNumber, cache[pos], 0);
		dirty[pos] = false;
		sectors[pos] = sectorNumber;
	}
	
	public static void close(){
		for(int i = 0; i < cacheNumber; ++i){
			if(dirty[i]){
				Machine.synchDisk().writeSector(sectors[i], cache[i], 0);
			}
		}
	}
	
	public static int readSector(int sectorNumber, int sectorOffset, byte[] buffer, int offset, int length)
  	{
		Lib.assertTrue(offset >= 0 && length >= 0 && offset + length <= buffer.length);
		
		int amount = Math.min(length, Disk.SectorSize - sectorOffset);
		//concurCtrler.beginRead();
		lock.acquire();
		int pos = sectorInCache(sectorNumber);
		while(pos == -1){
			pos = pickReplaced();
			replace(pos, sectorNumber);
		}
		readFromCache(pos, sectorOffset, buffer, offset, amount);
		lock.release();
		//concurCtrler.endRead();
		return amount;
		/*
		byte[] sectorContent = new byte[Disk.SectorSize]; // here may use cache to accelerate
		
	  	Machine.synchDisk().readSector(sectorNumber, sectorContent, 0);
	  	
	  	System.arraycopy(sectorContent, sectorOffset, buffer, offset, amount);
	  	
	  	return amount;
	  	*/
  	}
	
	public static int writeSector(int sectorNumber, int sectorOffset, byte[] buffer, int offset, int length)
	{
		Lib.assertTrue(offset >= 0 && length >= 0 && offset + length <= buffer.length);
		int amount = Math.min(length, Disk.SectorSize - sectorOffset);
		//concurCtrler.beginWrite();
		lock.acquire();
		int pos = sectorInCache(sectorNumber);
		if(pos == -1){
			pos = pickReplaced();
			replace(pos, sectorNumber);
		}
		writeToCache(pos, sectorOffset, buffer, offset, amount);
		lock.release();
		//concurCtrler.endWrite();
		return amount;
		/*
		byte[] sectorContent = new byte[Disk.SectorSize];
		
		Machine.synchDisk().readSector(sectorNumber, sectorContent, 0);
		System.arraycopy(buffer, offset, sectorContent, sectorOffset, amount);
		Machine.synchDisk().writeSector(sectorNumber, sectorContent, 0);
		
		return amount;
		*/
	}
	
	public static int readSector(int sectorNumber, byte[] buffer, int length){
		return readSector(sectorNumber, 0, buffer, 0, length);
	}
	
	public static int writeSector(int sectorNumber, byte[] buffer, int length){
		return writeSector(sectorNumber, 0, buffer, 0, length);
	}
	/*
	private static class ConcurrencyController {
		private Lock lock;
		private Condition cond;
		private int readCount, writeCount;

		public ConcurrencyController() {
			lock = new Lock();
			cond = new Condition(lock);
			readCount = 0;
			writeCount = 0;
		}

		public void beginRead() {
			lock.acquire();
			while (writeCount > 0)
				cond.sleep();
			++readCount;
			lock.release();
		}

		public void endRead() {
			lock.acquire();
			--readCount;
			if (readCount == 0)
				cond.wakeAll();
			lock.release();
		}

		public void beginWrite() {
			lock.acquire();
			while (writeCount > 0 || readCount > 0)
				cond.sleep();
			++writeCount;
			lock.release();
		}

		public void endWrite() {
			lock.acquire();
			--writeCount;
			cond.wakeAll();
			lock.release();
		}
	}*/
}
