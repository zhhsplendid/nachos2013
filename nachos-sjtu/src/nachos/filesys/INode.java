package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Lib;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class INode
{
  /** represent a system file (free list) */
  public static int TYPE_SYSTEM = 0;
  
  /** represent a folder */
  public static int TYPE_FOLDER = 1;
  
  /** represent a normal file */
  public static int TYPE_FILE = 2;
  
  /** represent a normal file that is marked as delete */
  public static int TYPE_FILE_DEL = 3;
  
  /** represent a symbolic link file */
  public static int TYPE_SYMLINK = 4;
  
  /** represent a folder that are not valid */
  public static int TYPE_FOLDER_DEL = 5;
  
  /** the reserve size (in byte) in the first sector */ //but until now not use
  private static final int FIRST_SEC_RESERVE = 16;
  
  public boolean ishead;
  
  public static final char dbgINode = 'i';
  
  public static int sectorOffsetLen = sectorOffsetLength();
  public static int sectorMask;
  /**
   * a function to initial sectorOffsetLen and sectorMask;
   * @return sector offset length
   */
  private static int sectorOffsetLength(){
	  int size = Disk.SectorSize;
	  sectorMask = size - 1;
	  int len = 0;
	  while(size > 1){
		  size >>= 1;
		  ++len;
	  }
	  return len;
  }
  
  /** size of the file in bytes */
  int file_size;
  
  /** the type of the file */
  int file_type;
  
  /** the number of programs that have access on the file */
  int use_count;
  
  /** the number of links on the file */
  int link_count;
  
  private static int addr_MAXN = 127; 
  
  /** maintain all the sector numbers this file used in order */
  private int[] sec_addr;
  
  /** the first address */
  protected int addr;
  
  /** the extended address */
  private int ext_inode;
  
  public INode (int addr)
  {
    file_size = 0;
    file_type = TYPE_FILE;
    use_count = 0;
    link_count = 0;
    sec_addr = new int[addr_MAXN];
    this.addr = addr;
    ext_inode = 0;
    ishead = true;
  }
  
  public static INode create(){ // default inode type is TYPE_FILE
	  int addr = FilesysKernel.realFileSystem.getFreeList().allocate();
	  INode inode = new INode(addr);
	  inode.createFileInit();
	  return inode;
  }
  
  public static INode inodeForCreateFile(int addr, int type){
	  INode inode = new INode(addr);
	  inode.file_type = type;
	  inode.createFileInit();
	  return inode;
  }
  /**
   * allocate first sector to a file to reserve 16-byte external information
   */
  public void createFileInit(){
	  Lib.assertTrue(ishead);
	  if(sec_addr[0] != 0){
		  Lib.debug(dbgINode, "maybe(but maybe!) wrong when allocate first sector to a file");
		  return;
	  }
	  if(addr == FreeList.STATIC_ADDR){
		  Lib.assertTrue(FreeList.SECTOR_BEGIN_FOR_USER > 3);
		  sec_addr[0] = FreeList.SECTOR_BEGIN_FOR_USER - 2;
		  sec_addr[1] = FreeList.SECTOR_BEGIN_FOR_USER - 1;
	  }
	  else{
		  sec_addr[0] = FilesysKernel.realFileSystem.getFreeList().allocate();
	  }
	  ++link_count;
	  save();
  } 
  /**
   * get a position's offset
   * 
   * @param pos
   * @return offset
   */
  public static int getOffset (int pos)
  {
	  return (pos + FIRST_SEC_RESERVE) & sectorMask;
  }
  
  
  /**
   * get a sector number and return its position;
   * @param sectorNumber
   * @return
   */
  /*
  public static int getNextSectorPosition(int pos){
	  return ((pos + FIRST_SEC_RESERVE) / Disk.SectorSize + 1) << sectorOffsetLen - FIRST_SEC_RESERVE;
  }*/
  
  /**
   * int put a size and get how many sectors it need to save a file with this size
   * @param size
   * @return
   */
  public static int getSectorCount(int size){
	  int textSectors = ((size + FIRST_SEC_RESERVE) >> sectorOffsetLen) + 1;
	  int inodeSectors = textSectors / addr_MAXN + 1;
	  return textSectors + inodeSectors;
  }
  /*
  public static int getPosition(int logicSectorNumber){
	  return (logicSectorNumber << sectorOffsetLen) - FIRST_SEC_RESERVE;
  }
  
  public static int getLogicSector(int pos){
	  return (pos + FIRST_SEC_RESERVE) >> sectorOffsetLen;
  }*/
  
  /**
   * return a pos is in which real sector
   * @param pos
   * @return 
   */
  public int getSector(int pos){
	  return getLogicSectorMap((pos + FIRST_SEC_RESERVE) >> sectorOffsetLen);
  }
  
  /** get the sector number of a position in the file  */
  public int getLogicSectorMap(int logicSector)
  {
	  if(logicSector < addr_MAXN){
		  if(sec_addr[logicSector] != 0){
			  return sec_addr[logicSector];
		  }
		  else{
			  return -1;
		  }
	  }
	  else if(ext_inode != 0){
		  INode next = loadNext();
		  return next.getLogicSectorMap(logicSector - addr_MAXN);
	  } 
	  return -1;
  }
  
  /**
   * get a free sectors until pos, save all got sector in logic sector
   * and return the first free sector it get;
   * @return
   */
  public int getFreeSector(int pos){
	  int logicSectorMax = (file_size + FIRST_SEC_RESERVE) >> sectorOffsetLen;
	  int newMax = (pos + FIRST_SEC_RESERVE) >> sectorOffsetLen;
	  int needSector = newMax - logicSectorMax;
	  
	  if(needSector <= 0){
		  return -1;
	  }
	  
	  int[] freeSector = FilesysKernel.realFileSystem.getFreeList().allocateSectors(needSector);
	  if(freeSector == null){
		  return -1;
	  }
	  
	  int amount = addFreeSector(logicSectorMax + 1, freeSector, 0);
	  Lib.assertTrue(amount == freeSector.length);
	  
	  save();
	  return freeSector[0];
  }
  
  /**
   * add sec_addr
   * can be flushed from index
   * @param index
   * @param freeSector
   * @param offset
   * @return
   */
  public int addFreeSector(int index, int[] freeSector, int offset){
	  int amount = Math.min(addr_MAXN - index, freeSector.length - offset);
	  if(amount < 0){
		  amount = 0;
	  }
	  
	  if(index < addr_MAXN){		
		  for(int i = 0; i < amount; ++i){
			  sec_addr[index + i] = freeSector[offset + i];  
		  }
		  save();
	  }
	  
	  if(amount < freeSector.length - offset){
		  INode next;
		  if(ext_inode == 0){
			  ext_inode = FilesysKernel.realFileSystem.getFreeList().allocate();
			  next = newNextINode();
			  saveExtINode();
		  }
		  else{
			  next = loadNext();
		  }
		  amount += next.addFreeSector(index + amount - addr_MAXN, freeSector, offset + amount);
	  }
	  if(amount != freeSector.length - offset){
		  Lib.debug(dbgINode, "in addFreeSector() amount not true");
	  }
	  return amount;
  } 
  
  
  private INode newNextINode(){
	  INode nextINode = new INode(ext_inode);
	  nextINode.ishead = false;
	  return nextINode;
  }
  /**
   * this is a function for load next inode easily
   * 
   * @param addr
   * @return
   */
  private INode loadNext(){
	  INode nextINode = new INode(ext_inode);
	  nextINode.ishead = false;
	  nextINode.load();
	  return nextINode;
  }
  
  /**
   * this is a function for load a inode easily
   * 
   * @param addr
   * @return
   */
  public static INode loadINode(int addr){
	INode load_inode = new INode(addr);
	load_inode.load();
	return load_inode;
  }
  
  /** change the file size and adjust the content in the inode accordingly */
  public void setFileSize (int size)
  {
	file_size = size;
	saveSize();
  }
  
  /** set the file type*/
  public void setFileType(int type)
  {
	  file_type = type;
	  saveType();
  }
  
  /** free the disk space occupied by the file (including inode) */
  public void free ()
  {
	if(ext_inode != 0){
		INode next = loadNext();
		next.free();
	}
	for(int i = 0; i < addr_MAXN; ++i){
		if(sec_addr[i] != 0){
			FilesysKernel.realFileSystem.getFreeList().deallocate(sec_addr[i]);
		}
		else{
			break;
		}
	}
	FilesysKernel.realFileSystem.getFreeList().deallocate(addr);
  }
  
  /**
   * a function free space from position to file size;
   * that's means file size be smaller;
   * @param pos
   */
  public void freeSector(int pos) {
	  int logicSectorMax = (file_size + FIRST_SEC_RESERVE) >> sectorOffsetLen;
	  int newMax = (pos + FIRST_SEC_RESERVE) >> sectorOffsetLen;
	  
      int needFree = logicSectorMax - newMax;
      if(needFree <= 0){
    	  return;
      }
      
      removeSector(newMax + 1, needFree);
  }
  
  /**
   * @param index from this index of this inode need free (sec_addr[index])
   * @param needFree how many sectors need to free
   * @return return how many sectors free successful
   */
  private int removeSector(int index, int needFree){
	 int amount = Math.min(addr_MAXN - index, needFree);
	 if(amount < 0){
		 amount = 0;
	 }
	 
	 if(index < addr_MAXN){
		 for(int i = 0; i < amount; ++i){
			 Lib.assertTrue(sec_addr[index + i] != 0);
			 FilesysKernel.realFileSystem.getFreeList().deallocate(sec_addr[index + i]);
			 sec_addr[index + i] = 0;
		 }
		 save();
	 }
	 if(amount < needFree){
		 Lib.assertTrue(ext_inode != 0);
		 int toRelease = 0;
		 if(amount != 0){
			 toRelease = ext_inode;
		 }
		 INode next = loadNext();
		 amount += next.removeSector(index + amount - addr_MAXN, needFree - amount);
		 
		 if(toRelease != 0){
			 FilesysKernel.realFileSystem.getFreeList().deallocate(toRelease);
			 ext_inode = 0;
		 }
		 saveExtINode();
	 }
	 if(amount != needFree){
		  Lib.debug(dbgINode, "in removeSector() amount not true");
	  }
	 return amount;
  }
  
  /** load inode content from the disk */
  public void load ()
  {
     byte[] buffer = new byte[Disk.SectorSize];
     DiskManager.readSector(addr, buffer, Disk.SectorSize);
     
     ext_inode = Lib.bytesToInt(buffer, 0);
     for(int i = 0; i < addr_MAXN; ++i){
    	 sec_addr[i] = Lib.bytesToInt(buffer, 4 + 4 * i);
     }
     
     if(ishead){
		 byte[] data = new byte[FIRST_SEC_RESERVE];
		 DiskManager.readSector(sec_addr[0], data, FIRST_SEC_RESERVE);
		 file_size = Lib.bytesToInt(data, 0);
		 file_type = Lib.bytesToInt(data, 4);
		 link_count = Lib.bytesToInt(data, 8);
		 use_count = Lib.bytesToInt(data, 12);
	 }
     
  }
  
  /** save inode content to the disk */
  public void saveSize(){
	  if(ishead){
		  byte[] data = Lib.bytesFromInt(file_size); 
		  Lib.assertTrue(sec_addr[0] != 0, "save inode wrong");
		  DiskManager.writeSector(sec_addr[0], 0, data, 0, 4);
	  }
  }
  public void saveType(){
	  if(ishead){
		  byte[] data = Lib.bytesFromInt(file_type); 
		  Lib.assertTrue(sec_addr[0] != 0, "save inode wrong");
		  DiskManager.writeSector(sec_addr[0], 4, data, 0, 4);
	  }
  }
  public void saveLinkCount(){
	  if(ishead){
		  byte[] data = Lib.bytesFromInt(link_count); 
		  Lib.assertTrue(sec_addr[0] != 0, "save inode wrong");
		  DiskManager.writeSector(sec_addr[0], 8, data, 0, 4);
	  }
  }
  public void saveUseCount(){
	  if(ishead){
		  byte[] data = Lib.bytesFromInt(use_count); 
		  Lib.assertTrue(sec_addr[0] != 0, "save inode wrong");
		  DiskManager.writeSector(sec_addr[0], 12, data, 0, 4);
	  }
  }
  
  private void saveExtINode(){
	  byte[] data = Lib.bytesFromInt(ext_inode);
	  DiskManager.writeSector(addr, 0, data, 0, 4);
  }
  public void save ()
  {
	 
	 if(ishead){
		 byte[] data = new byte[FIRST_SEC_RESERVE];
		 Lib.bytesFromInt(data, 0, file_size);
		 Lib.bytesFromInt(data, 4, file_type);
		 Lib.bytesFromInt(data, 8, link_count);
		 Lib.bytesFromInt(data, 12, use_count);
		 
		 Lib.assertTrue(sec_addr[0] != 0, "save inode wrong");
		 DiskManager.writeSector(sec_addr[0], data, FIRST_SEC_RESERVE);
	 }
	 
	 byte[] buffer = new byte[Disk.SectorSize];
	 Lib.bytesFromInt(buffer, 0, ext_inode);
	 int count;
	 for(count = 0; count < addr_MAXN; ++count){
	   if(sec_addr[count] == 0){
		   break;
	   }
	   Lib.bytesFromInt(buffer, 4 + 4 * count, sec_addr[count]);
	 }
	 DiskManager.writeSector(addr, buffer, Disk.SectorSize);
  }
}
