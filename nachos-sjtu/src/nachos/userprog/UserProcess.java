package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;

import java.io.EOFException;
import java.util.*;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
		int numPhysPages = Machine.processor().getNumPhysPages();
		pageTable = new TranslationEntry[numPhysPages];
		for (int i = 0; i < numPhysPages; i++)
			pageTable[i] = new TranslationEntry(i, 0, false, false, false, false);
		numPages = 0;
		
		processLock = new Lock(); 
		childProcesses = new HashSet<UserProcess>();
		
		processLock.acquire();
		pid = ++processesCount;
		globalProcesses.put(pid, this);
		processLock.release();
		
		fileTable = new HashMap<Integer, OpenFile>();
		
		fileTable.put(0, UserKernel.console.openForReading());
		fileTable.put(1, UserKernel.console.openForWriting());
		fileId = 2;
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
		if (!load(name, args))
			return false;
		// I add codes here
		processLock.acquire();
		++activeProcesses;
		processLock.release();
		
		fatherProcess = UserKernel.currentProcess();
		//
		new UThread(this).setName(name).fork();

		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		Machine.processor().setPageTable(pageTable);
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();

		/* for now, just assume that virtual addresses equal physical addresses
		if (vaddr < 0 || vaddr >= memory.length)
			return 0;
			
		int amount = Math.min(length, memory.length - vaddr);
		System.arraycopy(memory, vaddr, data, offset, amount);
		
		return amount;
		*/
		int vpn = vaddr >> offsetLen;
		TranslationEntry translationEntry = getTranslationEntry(vpn);
		if(translationEntry == null || !translationEntry.valid){
			return 0;
		}
		int addrOffset = vaddr & offsetMask;
		int phyAddr = (translationEntry.ppn << offsetLen) | addrOffset;
		
		int amount = Math.min(length, pageSize - addrOffset);
		System.arraycopy(memory, phyAddr, data, offset, amount);
		
		if(amount < length){
			return amount + readVirtualMemory((vpn+1) << offsetLen, data, offset + amount, length - amount);
		}
		else return amount;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();

		/* for now, just assume that virtual addresses equal physical addresses
		if (vaddr < 0 || vaddr >= memory.length)
			return 0;

		int amount = Math.min(length, memory.length - vaddr);
		System.arraycopy(data, offset, memory, vaddr, amount);

		return amount;
		*/
		int vpn = vaddr >> offsetLen;
		TranslationEntry translationEntry = getTranslationEntry(vpn);
		if(translationEntry == null || !translationEntry.valid || translationEntry.readOnly){
			return 0;
		}
		int addrOffset = vaddr & offsetMask;
		int phyAddr = (translationEntry.ppn << offsetLen) | addrOffset;
		
		int amount = Math.min(length, pageSize - addrOffset);
		System.arraycopy(data, offset, memory, phyAddr, amount);
		
		if(amount < length){
			return amount + writeVirtualMemory((vpn+1) << offsetLen, data, offset + amount, length - amount);
		}
		else return amount;
	}

	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			//numPages += section.getLength();
			//I change codes here
			if(!acquirePhyPages(numPages, section.getLength(), section.isReadOnly())){
				unloadSections();
				coff.close();
				Lib.debug(dbgProcess, "acquire section page wrong");
				return false;
			}
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		//numPages += stackPages;
		if(!acquirePhyPages(numPages, stackPages, false)){
			unloadSections();
			coff.close();
			Lib.debug(dbgProcess, "acquire stack pages wrong");
			return false;
		}
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		//numPages++;
		if(!acquirePhyPages(numPages, 1, false)){
			unloadSections();
			coff.close();
			Lib.debug(dbgProcess, "acquire reserve pages wrong");
		}

		if (!loadSections())
			return false;

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib.assertTrue(writeVirtualMemory(entryOffset,stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib.assertTrue(writeVirtualMemory(stringOffset,new byte[] { 0 }) == 1);
			stringOffset += 1;
		}

		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
		if (numPages > Machine.processor().getNumPhysPages()) {
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			return false;
		}

		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;

				/* for now, just assume virtual addresses=physical addresses
				section.loadPage(i, vpn);
				*/
				TranslationEntry translationEntry = getTranslationEntry(vpn);
				if(translationEntry == null){
					return false;
				}
				section.loadPage(i, translationEntry.ppn);
			}
		}

		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		for(int i = 0; i < numPages; ++i){
			if(pageTable[i].valid){
				UserKernel.releasePage(pageTable[i]);
			}
		}
		numPages = 0;
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

	/**
	 * Handle the void halt() system call.
	 * 
	 * Halt the Nachos machine by calling Machine.halt(). Only the root process
	 * (the first process, executed by UserKernel.run()) should be allowed to
	 * execute this syscall. Any other process should ignore the syscall and return
	 * immediately.
	 */
	private int handleHalt() {
		if(pid == 1){
			releaseAll();
			processLock.acquire();
			--activeProcesses;
			processLock.release();
			
			Machine.halt();

			Lib.assertNotReached("Machine.halt() did not halt machine!");
		}
		return 0;
	}
	/**
	 * Handle the int creat(char *name) system call.
	 * Open the named disk file, creating it if it does not exist.
	 * 
	 * @param fileNameAddr
	 * 			a integer to read file name in virtual memory 
	 * @return 
	 * 			fileDiscriptor, -1 if error happens
	 */
	private int handleCreate(int fileNamePoint){
		String fileName = readVirtualMemoryString(fileNamePoint, maxStringLength);
		if(fileName == null){
			Lib.debug(dbgProcess, "read file name error");
			return -1;
		}
		
		OpenFile ofile = ThreadedKernel.fileSystem.open(fileName, true);
		if(ofile == null){
			Lib.debug(dbgProcess, "create file " + fileName + " error");
			return -1;
		}
		
		fileTable.put(fileId, ofile);
		
		return fileId++;
	}
	
	/**
	 * Handle the int open(char *name) system call.
	 * Open the named file and return a file descriptor.
	 * 
	 * @param fileNameAddr
	 * 			a integer to read file name in virtual memory 
	 * @return 
	 * 			fileDiscriptor, -1 if error happens
	 */
	private int handleOpen(int fileNamePoint){
		String fileName = readVirtualMemoryString(fileNamePoint, maxStringLength);
		if(fileName == null){
			Lib.debug(dbgProcess, "read file name error");
			return -1;
		}
		
		OpenFile ofile = ThreadedKernel.fileSystem.open(fileName, false);
		if(ofile == null){
			Lib.debug(dbgProcess, "open file " + fileName + " error");
			return -1;
		}
		
		fileTable.put(fileId, ofile);
		
		return fileId++;
	}
	
	/**
	 * Handle the int read(int fileDescriptor, void *buffer, int count) system call.
	 * Read up to count bytes into buffer from the file or stream referred to by fileDescriptor.
	 * 
	 * @param fileDescriptor
	 * @param bufferPoint
	 * 			Address to write in virtual memory 
	 * @param length
	 * 			bytes count to read from the file
	 * @return
	 */
	private int handleRead(int fileDescriptor, int bufferPoint, int length){
		OpenFile ofile = fileTable.get(fileDescriptor);
		if(ofile == null){
			Lib.debug(dbgProcess, "not such file in the process called system call read()");
			return -1;
		}
		
		byte[] buffer = new byte[length];
		
		int bytesRead = ofile.read(buffer, 0, length);
		if(bytesRead == - 1){
			Lib.debug(dbgProcess, "return -1 when call OpenFile.read() in system call read()");
			return -1;
		}
		
		int bytesWrite = writeVirtualMemory(bufferPoint, buffer, 0, bytesRead);
		if(bytesWrite != bytesRead){
			Lib.debug(dbgProcess, "can't write whole buffer into memory");
			return -1;
		}
		
		return bytesWrite;
	}
	
	/**
	 * Handle the int write(int fileDescriptor, void *buffer, int count) system call.
	 * write up to count bytes from buffer to the file or stream referred to by fileDescriptor.
	 * @param fileDescriptor
	 * @param bufferPoint
	 * 			Address to write in virtual memory 
	 * @param length
	 * 			bytes count to read from the file
	 * @return
	 */
	private int handleWrite(int fileDescriptor, int bufferPoint, int length){
		OpenFile ofile = fileTable.get(fileDescriptor);
		if(ofile == null){
			Lib.debug(dbgProcess, "not such file in the process called system call write()");
			return -1;
		}
		
		byte[] buffer = new byte[length];
		
		int bytesRead = readVirtualMemory(bufferPoint, buffer);
		if(bytesRead != length){
			Lib.debug(dbgProcess, "can't read length in system call write()");
			return -1;
		}
		
		int bytesWrite = ofile.write(buffer, 0, length);
		
		return bytesWrite;
	}
	
	/**
	 * Close a file descriptor, so that it no longer refers to any file or stream
	 * and may be reused.
	 * 
	 * @param fileDescriptor
	 * @return 0 success or -1 failed
	 */
	private int handleClose(int fileDescriptor){
		OpenFile ofile = fileTable.get(fileDescriptor);
		if(ofile == null){
			Lib.debug(dbgProcess, "system call close() no file descriptor exsits");
			return -1;
		}
		
		ofile.close();
		fileTable.remove(fileDescriptor);
		
		return 0;
	}
	
	/**
	 * Delete a file from the file system. If no processes have the file open, the
	 * file is deleted immediately and the space it was using is made available for
	 * reuse.
	 *
	 * If any processes still have the file open, the file will remain in existence
	 * until the last file descriptor referring to it is closed. However, creat()
	 * and open() will not be able to return new file descriptors for the file
	 * until it is deleted. <b>I will implement this in fileSystem.remove </b>
	 *
	 * @param fileNamePoint
	 * @return 0 on success, or -1 if an error occurred.
	 */
	private int handleUnlink(int fileNamePoint){
		String fileName = readVirtualMemoryString(fileNamePoint, maxStringLength);
		if(fileName == null){
			Lib.debug(dbgProcess, "read file name error");
			return -1;
		}
		
		if(ThreadedKernel.fileSystem.remove(fileName)){
			return 0;
		}
		return -1;
	}
	/**
	 * Handle the void exit(int status)
	 * 
	 * @param status
	 * @return
	 */
	private int handleExit(int status){
		releaseAll();
		exitStatus = status;
		
		boolean halt = false;
		processLock.acquire();
		
		--activeProcesses;
		if(activeProcesses == 0){
			halt = true;
		}
		processLock.release();
		
		if(halt){
			Machine.halt();
		}
		else UThread.finish();
		
		return 0;
	}
	
	/**
	 * Handle the int exec(char *name, int argc, char **argv);
	 * 
	 * @param fileNamePoint
	 * @param argc
	 * @param argvPoint
	 * @return
	 */
	private int handleExec(int fileNamePoint, int argc, int argvPoint){
		String fileName = readVirtualMemoryString(fileNamePoint, maxStringLength);
		if(fileName == null){
			Lib.debug(dbgProcess, "read file name error in system call exec()");
			return -1;
		}
		if(argc < 0){
			Lib.debug(dbgProcess, "not enough argc in exec()");
			return -1;
		}
		
		String[] arguments = new String[argc];
		for(int i = 0; i < argc; ++i){
			byte[] buffer = new byte[4];
            if (readVirtualMemory(argvPoint + i * 4, buffer) != buffer.length){
            	Lib.debug(dbgProcess, "read argument address error in system call exec()");
                return -1;
            }
            int addr = Lib.bytesToInt(buffer, 0);
            arguments[i] = readVirtualMemoryString(addr, maxStringLength);
            if (arguments[i] == null){
            	Lib.debug(dbgProcess, "read arguments error in system call exec()");
                return -1;
            }
		}
		UserProcess childProcess = UserProcess.newUserProcess();
		if(!childProcess.execute(fileName, arguments)){
			Lib.debug(dbgProcess, "execute error in system call exec()");
			return -1;
		}
		return childProcess.pid;
	}
	
	/**
	 * handle int join(int pid, int *status);
	 * 
	 * @param processID
	 * @param statusPoint
	 * @return
	 */
	private int handleJoin(int processID, int statusPoint){
		 UserProcess childProcess = globalProcesses.get(processID);
		 if(childProcess == null){
			 Lib.debug(dbgProcess, "join() invalid process id");
			 return -1;
		 }
		 if(childProcess.fatherProcess != this){
			 Lib.debug(dbgProcess, "join() child process' father wrong");
			 return -1;
		 }
		 
		 childProcess.thread.join();
		 childProcess.fatherProcess = null;
		 childProcesses.remove(childProcess);
		 
		 if(childProcess.hasExit){
			 writeVirtualMemory(statusPoint, Lib.bytesFromInt(childProcess.exitStatus));
			 return 1;
		 }
		 else return 0;
	}
	
	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
     * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		//System.out.println("syscall = " + syscall);
		switch (syscall) {
		case syscallHalt:
			return handleHalt();
		
		case syscallExit:
			return handleExit(a0);
		case syscallExec:
			return handleExec(a0, a1, a2);
		case syscallJoin:
			return handleJoin(a0, a1);
			
		case syscallCreate:
			return handleCreate(a0);
		case syscallOpen:
			return handleOpen(a0);
		case syscallRead:
			return handleRead(a0, a1, a2);
		case syscallWrite:
			return handleWrite(a0, a1, a2);
		case syscallClose:
			return handleClose(a0);
		case syscallUnlink:
			return handleUnlink(a0);
		default:
			Lib.debug(dbgProcess, "Unknown syscall " + syscall);
			handleKill(unknowSystemCallException);
			Lib.assertNotReached("Unknown system call!");
		}
		return 0;
	}
	
	public int handleKill(int status){
		releaseAll();
		//System.out.println("kill status = " + status);
		exitStatus = status;
		hasExit = false; // kill this process, not exit! 
		
		boolean halt = false;
		processLock.acquire();
		
		--activeProcesses;
		if(activeProcesses == 0){
			halt = true;
		}
		processLock.release();
		
		if(halt){
			Machine.halt();
		}
		else UThread.finish();
		
		return 0;
	}
	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0), processor
							.readRegister(Processor.regA1), processor
							.readRegister(Processor.regA2), processor
							.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			break;

		default:
			Lib.debug(dbgProcess, "Unexpected exception: "
					+ Processor.exceptionNames[cause]);
			handleKill(cause);
			Lib.assertNotReached("Unexpected exception");
		}
	}
	
	/**
	 * get TranslationEntry in the pageTable though vpn;
	 */
	protected TranslationEntry getTranslationEntry(int vpn){
		if(vpn >= 0 && vpn < pageTable.length){
			return pageTable[vpn];
		}
		return null;
	}
	/**
	 * acquire Physical pages in pageTable
	 * 
	 * @param firstVPN
	 * @param needNum
	 * @param readOnly
	 * @return true if success or false if failed
	 */
	protected boolean acquirePhyPages(int firstVPN, int needNum, boolean readOnly){
		int[] pagesId = UserKernel.allocatePages(needNum);
		if(pagesId == null || pagesId.length != needNum){
			Lib.debug(dbgProcess, "allocatePages wrong");
			return false;
		}
		for(int i = 0; i < needNum; ++i){
			pageTable[firstVPN + i] = new TranslationEntry
					(firstVPN+1, pagesId[i], true, readOnly, false, false);
		}
		numPages += needNum; 
		return true;
	}
	
	/**
	 * called before halt or exit
	 */
	protected void releaseAll(){
		unloadSections();
		
		for(OpenFile ofile: fileTable.values()){
			ofile.close();
		}
		
		for(UserProcess process: childProcesses){
			process.fatherProcess = null;
		}
		
		coff.close();
		
		hasExit = true;
	}
	
	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;

	/** The number of pages in the program's stack. */
	protected final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	private int initialPC, initialSP;
	private int argc, argv;

	private static final int pageSize = Processor.pageSize;
	protected static int offsetLen = getOffsetLength();
	protected static int offsetMask;
	protected static int getOffsetLength(){
		int ans = 0;
		while(true){
			if( (pageSize >> ans) == 1) break;
			++ans;
		}
		offsetMask = (1 << ans) - 1;
		return ans;
	}
	private static final char dbgProcess = 'a';
	private static final int unknowSystemCallException = 999;
	
	/** Nachos arguments of string maximun length*/
	protected static final int maxStringLength = 256;
	
	/** This process's id -- identifier*/
	protected int pid;
	protected static int processesCount = 0;
	protected static int activeProcesses = 0;
	/** Open file's id of this process*/ 
	protected int fileId;
	
	protected int exitStatus = 0;
	protected boolean hasExit = false;
	
	/** This process may use lock */
	protected Lock processLock;
	
	public UserProcess fatherProcess;
	
	protected UThread thread;
	
	/** files opened by this process*/
	protected HashMap<Integer, OpenFile> fileTable;
	
	protected static HashMap<Integer, UserProcess> globalProcesses = new HashMap<Integer, UserProcess>();;
	protected HashSet<UserProcess> childProcesses;
}
