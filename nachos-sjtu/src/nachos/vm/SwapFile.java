package nachos.vm;

import nachos.machine.*;
import nachos.threads.*;

import java.util.*;

/**
 * SwapFile is a class to manage file "SWAP"
 * in order to manage Virtual Page move between disk and physical memory
 * @author Splendid Zheng
 *
 */
public class SwapFile {
	
	private SwapFile(){
		pageSize = Processor.pageSize;
		swapFile = ThreadedKernel.fileSystem.open(swapFileName, true);
		
		positionOnFile = new HashMap<ITableItem, Integer>();
		freeSegment = new LinkedList<Integer>();
		maxFilePos = 0;
	}
	
	/**
	 * to make sure only one swap file
	 * @return
	 */
	public static SwapFile getInstance(){
		//swapFileLock.acquire();
		if(instance == null){
			instance = new SwapFile();
		}
		//swapFileLock.release();
		return instance;
	}
	
	/**
	 * for a page to get the save position on swap file
	 * if page have already have a given position, return it
	 * else allocate a space to this page;
	 * 
	 * allocate first search if there has holes
	 * if not, return a new position maxFilePos
	 * and maxFilePos += 1
	 * 
	 * @param pid
	 * @param vpn
	 * @return save position on swap file
	 */
	public int getPos(int pid, int vpn){
		ITableItem item = new ITableItem(pid, vpn);
		
		//swapFileLock.acquire();
		Integer pos = positionOnFile.get(item);
		if(pos != null){
			//swapFileLock.release();
			return pos.intValue();
		}
		
		if(freeSegment.isEmpty()){
			positionOnFile.put(item, maxFilePos);
			//swapFileLock.release();
			return maxFilePos++;
		}
		else{
			positionOnFile.put(item, freeSegment.getFirst());
			int seg = freeSegment.poll();
			//swapFileLock.release();
			return seg;
		}
	}
	
	/**
	 * move the page <pid, vpn> from memory into disk, in the other word, swap file.
	 * 
	 * @param pid
	 * @param vpn
	 * @param ppn
	 */
	public void PageWriteToFile(int pid, int vpn, int ppn){
		//swapFileLock.acquire();
		int pos = getPos(pid, vpn);
		
		byte[] memory = Machine.processor().getMemory();
		swapFile.write(pos * pageSize, memory, ppn*pageSize, pageSize);
		/*
		byte[] copy = new byte[pageSize];//inorder to avoid change memory when in swapFile.write
		System.arraycopy(memory, ppn*pageSize, copy, 0, pageSize);
		swapFile.write(pos * pageSize, copy, 0, pageSize);
	
		boolean flag = false;
		byte[] cmp = new byte[pageSize];
		swapFile.read(pos * pageSize, cmp, 0, pageSize);
		
		for(int i = 0; i < pageSize; ++i){
			if(cmp[i] != copy[i]){
				flag = true;
			}
		}
		if(flag){
			System.out.println("ppn = " + ppn + " pos = " + pos + " test pos = " + pos*pageSize);
			for(int i = 0; i < 32; ++i){
				for(int j = 0; j < 32; ++j){
					System.out.print(copy[i * 32 + j] + " ");
				}
				System.out.println();
			}
			System.out.println("\n\n\n");
			for(int i = 0; i < 32; ++i){
				for(int j = 0; j < 32; ++j){
					System.out.print(cmp[i * 32 + j] + " ");
				}
				System.out.println();
			}
			Machine.halt();
		}*/
		//int writeLength = swapFile.write(pos * pageSize, memory, ppn * pageSize, pageSize);
		//Lib.assertTrue(writeLength == pageSize, "not reach write");
		//swapFileLock.release();
	}
	
	/**
	 * move the page <pid, vpn> from disk(swap file) into memory.
	 * @param pid
	 * @param vpn
	 */
	public void PageReadFromFile(int pid, int vpn, int ppn){
		ITableItem item = new ITableItem(pid, vpn);
		
		//swapFileLock.acquire();
		Integer position = positionOnFile.get(item);
		
		byte[] memory = Machine.processor().getMemory();
		
		for(int i = 0; i < pageSize; ++i){
			memory[ppn * pageSize + i] = 0;
		}
		if(position == null){	
			//swapFileLock.release();
			return; // this is because sometimes when we get a not dirty and don't need to write in swap file
		}
		
		swapFile.read(position * pageSize, memory, ppn * pageSize, pageSize);
		//swapFileLock.release();
	}
	
	/**
	 * remove the page <pid, vpn> from swap file, free the space it allcated.
	 * the free space will be add into freeSegment
	 * @param pid
	 * @param vpn
	 */
	public void removeFromDisk(int pid, int vpn){
		//swapFileLock.acquire();
		Integer pos = positionOnFile.remove(new ITableItem(pid, vpn));
		if(pos != null){
			freeSegment.add(pos);
		}
		//swapFileLock.release();
	}
	
	/**
	 * close the swap file
	 */
	public void close(){
		if(swapFile != null){
			swapFile.close();
			ThreadedKernel.fileSystem.remove(swapFileName);
			swapFile = null;
		}
	}
	
	/**
	 * return the private OpenFile swapFile
	 * @return swapFile
	 */
	public OpenFile getOpenSwapFile(){
		return swapFile;
	}
	
	private static SwapFile instance;
	
	private int pageSize;
	private String swapFileName = Config.getString("swapFile", "SWAP");
	
	private OpenFile swapFile;
	
	/**page(recognized as ITableItem) and its save space position on swap file*/
	private HashMap<ITableItem, Integer> positionOnFile;
	
	/**Segment leaved by process over and free their page space on disk*/
	private LinkedList<Integer> freeSegment;
	
	/**Swap file allocate max position for page*/
	private int maxFilePos;
	//private static Lock swapFileLock = new Lock();
}
