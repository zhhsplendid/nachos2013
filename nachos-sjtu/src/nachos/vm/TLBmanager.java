package nachos.vm;

import nachos.machine.*;
import nachos.threads.*;

import java.util.*;

public class TLBmanager {
	
	/**
	 * empty construct
	 */
	public TLBmanager() { 
		
	}
	
	/**
	 * pick TLB to be replace by random, maybe modified.
	 * @return TLB number to be replaced
	 */
	public static int pickReplaceTLB(){
		
		return LRUPick();
		//return randomPick();
	}
	
	/**
	 * random pick TLB to replace
	 * @return index for TLB to be replaced
	 */
	public static int randomPick(){
		//TLBLock.acquire();
		for(int i = 0; i < Machine.processor().getTLBSize(); ++i){
			if(Machine.processor().readTLBEntry(i).valid == false){
				//TLBLock.release();
				return i;
			}
		}
		//TLBLock.release();
		return Lib.random(Machine.processor().getTLBSize());
	}
	
	/**
	 * LRU(less recently use) pick TLB to be replaced
	 * @return index to be replaced
	 */
	public static int LRUPick(){
		for(int i = 0; i < Machine.processor().getTLBSize(); ++i){
			if(Machine.processor().readTLBEntry(i).valid == false){
				//TLBLock.release();
				return i;
			}
		}
		return LRU.getLast();
	}
	
	/**
	 * Let TLBEntry replace the entry in TLB[index]
	 * 
	 * @param pid
	 * @param index
	 * @param TLBEntry
	 */
	public static void replaceTLBEntry(int pid, int index, TranslationEntry newEntry){
		//TLBLock.acquire();
		TranslationEntry TLBEntry = Machine.processor().readTLBEntry(index);
		if(TLBEntry.valid){
			PageTable.getInstance().updateEntry(pid, TLBEntry);
		}
		Machine.processor().writeTLBEntry(index, newEntry);
		LRU.remove(new Integer(index));
		LRU.addFirst(index);
		//TLBLock.release();
	}
	
	/**
	 * To make the input entry in TLB(if it is in) write back to page table and invalid it
	 * called only in VMProcess.java private void movePageToDisk(int pid, TranslationEntry entry)
	 * 
	 * @param pid
	 * @param entry
	 */
	public static void flushTLB(int pid, TranslationEntry entry){
		Processor processor = Machine.processor();
		//TLBLock.acquire();
		
		for(int i = 0; i < processor.getTLBSize(); ++i){
			TranslationEntry tlbEntry = processor.readTLBEntry(i);
			if(tlbEntry.valid && tlbEntry.vpn == entry.vpn && tlbEntry.ppn == entry.ppn){

				PageTable.getInstance().updateEntry(pid, tlbEntry);
				
				tlbEntry.valid = false;
				processor.writeTLBEntry(i, tlbEntry);
				
				break;
			}
		}
		//TLBLock.release();
	}
	
	/**LinkedList for LRU(less recently use) algorithm*/
	static LinkedList<Integer> LRU = new LinkedList<Integer>();
	//private static Lock TLBLock = new Lock();
}
