package nachos.vm;

/**
 * this class is simply save two integer
 * and make it equals in HashMap key-value
 * 
 * full name is "Inverted Table Item"
 * to see invertedTable in nachos-sjtu/vm/PageTable.java
 * @author Splendid Zheng
 *
 */
public class ITableItem {
	public int processID = 0;
	public int vpn = 0;
	
	public ITableItem(int pid, int virtualPageNumber){
		processID = pid;
		vpn = virtualPageNumber;
	}
	
	/**
	 * implement boolean Object.equals()
	 */
	public boolean equals(Object o){
		if(o == null || !(o instanceof ITableItem)){
			return false;
		}
		return processID == ((ITableItem)o).processID && vpn == ((ITableItem)o).vpn;
	}
	
	/**
	 * implement the same hashCode for two equal ITableItem
	 */
	public int hashCode(){
		return (new Integer(processID).toString() + new Integer(vpn).toString()).hashCode();
	}
}
