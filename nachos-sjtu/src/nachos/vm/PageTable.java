package nachos.vm;

import nachos.machine.*;
import nachos.threads.Lock;

import java.util.*;

/**
 * a class for work set clock algorithm to pick page to replaced
 * 
 * @author Splendid Zheng
 *
 */
class WorksetClockItem {
	public int processID = 0;
	public long lastTime = 0;
	public TranslationEntry translationEntry = null;
	
	WorksetClockItem(int pid, long time, TranslationEntry entry){
		processID = pid;
		lastTime = time;
		translationEntry = entry;
	}
}

public class PageTable {
	
	/**
	 * private constructor to only construct one PageTable;
	 */
	private PageTable(){
		invertedTable = new HashMap<ITableItem, TranslationEntry>();
		coreMap = new WorksetClockItem[Machine.processor().getNumPhysPages()];
		clockHand = coreMap.length;
	}
	
	/**
	 * call the private constructor to get PageTable;
	 * @return the only PageTable
	 */
	public static PageTable getInstance(){
		
		//pageTableLock.acquire();
		
		if(instance == null){
			instance = new PageTable();
		}
		//pageTableLock.release();
		
		return instance;
	}
	
	/**
	 * get TranslationEntry in pageTable
	 * 
	 * @param processID
	 * @param vpn
	 * @return TranslationEntry in pagetable;
	 */
	public TranslationEntry getPageTableEntry(int processID, int vpn){
		//pageTableLock.acquire();
		
		TranslationEntry entry = invertedTable.get(new ITableItem(processID, vpn));
		if(entry != null){
			entry = new TranslationEntry(entry);
		}
		//pageTableLock.release();
		
		return entry;
	}
	
	/**
	 * add TranslationEntry in pageTable
	 * 
	 * @param processID
	 * @param entry
	 * @return true for success or false for fail
	 */
	public boolean addEntry(int processID, TranslationEntry entry){
		ITableItem item = new ITableItem(processID, entry.vpn);
		//pageTableLock.acquire();
		
		
		if(invertedTable.containsKey(item)){
			//pageTableLock.release();
			
			return false;
		}
		
		TranslationEntry newEntry = new TranslationEntry(entry);
		invertedTable.put(item, newEntry);
		if(newEntry.valid){
			coreMap[newEntry.ppn] = new WorksetClockItem(processID, Machine.timer().getTime(), newEntry);
		}
		//pageTableLock.release();
		
		return true;
	}
	
	/**
	 * remove Entry in page table
	 * 
	 * @param pid
	 * @param vpn
	 * @return the TranslationEntry to be removed
	 */
	public TranslationEntry removeEntry(int pid, int vpn) {
		
		//pageTableLock.acquire();
		
        TranslationEntry entry = invertedTable.remove(new ITableItem(pid, vpn));
        if (entry != null && entry.valid){
            coreMap[entry.ppn] = null;
        }
        //pageTableLock.release();
        
        return entry;
    }
	
	/**
	 * set the page table entry
	 * 
	 * @param pid
	 * @param entry
	 */
	public void setEntry(int pid, TranslationEntry entry){
		
		ITableItem item = new ITableItem(pid, entry.vpn);
		
		//pageTableLock.acquire();
		
		if(!invertedTable.containsKey(item)){
			//pageTableLock.release();
			return;
		}
		TranslationEntry oldEntry = invertedTable.get(item);
		TranslationEntry newEntry = new TranslationEntry(entry);
		
		if (oldEntry.valid) {
            Lib.assertTrue(coreMap[oldEntry.ppn] != null);
            coreMap[oldEntry.ppn] = null;
        }
        if (entry.valid) {
            Lib.assertTrue(coreMap[entry.ppn] == null);
            coreMap[entry.ppn] = new WorksetClockItem(pid, Machine.timer().getTime(), newEntry);
        }
        
        invertedTable.put(item, newEntry);
        //pageTableLock.release();
	}
	
	/**
	 * used by updateEntry, that is used |= used, dirty |= dirty
	 * @param e1
	 * @param e2
	 * @return
	 */
	private TranslationEntry combine(TranslationEntry e1, TranslationEntry e2) {
        TranslationEntry combinedEntry = new TranslationEntry(e1);
        if (e1.used || e2.used){
        	combinedEntry.used = true;
        }
        if (e1.dirty || e2.dirty){
        	combinedEntry.dirty = true;
        }
        return combinedEntry;
    }
	
	/**
	 * update Entry in pageTable, that is used |= used, dirty |= dirty
	 * 
	 * @param pid
	 * @param entry
	 */
	public void updateEntry(int pid, TranslationEntry entry){
		
		ITableItem item = new ITableItem(pid, entry.vpn);
		//pageTableLock.acquire();
		
		if(!invertedTable.containsKey(item)){
			//pageTableLock.release();
			
			return;
		}
		
		TranslationEntry oldEntry = invertedTable.get(item);
		TranslationEntry newEntry = combine(oldEntry, entry);
		
		if (oldEntry.valid) {
            Lib.assertTrue(coreMap[oldEntry.ppn] != null);
            coreMap[oldEntry.ppn] = null;
        }
        if (entry.valid) {
            Lib.assertTrue(coreMap[entry.ppn] == null);
            coreMap[entry.ppn] = new WorksetClockItem(pid, Machine.timer().getTime(), newEntry);
        }
        
        invertedTable.put(item, newEntry);
        //pageTableLock.release();
        
	}
	
	public WorksetClockItem pickPage(){
		return worksetPickPage();
		//return pickRandPage();
	}
	
	/**
	 * using rand to choose page to be replaced
	 * 
	 * @return
	 */
	public WorksetClockItem pickRandPage(){
		int pos = Lib.random(coreMap.length);
		//pageTableLock.acquire();
		
		WorksetClockItem randPick = coreMap[pos];
		//pageTableLock.release();
		return randPick;
	}
	/**
	 * using work set clock algorithm ro pick page to be replaced 
	 * 
	 * @return 
	 */
	public WorksetClockItem worksetPickPage(){
		//pageTableLock.acquire();
		int startPoint = (clockHand + 1) % coreMap.length;
		clockHand = startPoint;
		boolean needStart = true;
		long minTime = 1 << 60;
		long nowTime = Machine.timer().getTime();
		int toPick = -1;
		
		while(startPoint != clockHand || needStart){
			needStart = false;
			if(coreMap[clockHand] == null){
				Lib.assertNotReached("shouldn't have free page when call pick page to free");
				continue;
			}
			else if(coreMap[clockHand].translationEntry.used){
				coreMap[clockHand].lastTime = nowTime;
				coreMap[clockHand].translationEntry.used = false;
			}
			else if(nowTime - coreMap[clockHand].lastTime > TimeThreshold){
				//pageTableLock.release();
				return coreMap[clockHand];
			}
			else{
				if(coreMap[clockHand].lastTime < minTime){
					minTime = coreMap[clockHand].lastTime;
					toPick = clockHand;
				}
			}
			clockHand = (clockHand + 1) % coreMap.length;
		}
		//pageTableLock.release();
		
		if(toPick != -1) return coreMap[toPick];
		return coreMap[clockHand];
	}
	
	/**
	 * print CoreMap
	 */
	public void printCoreMap(){
		for(int i = 0; i < coreMap.length; ++i){
			if(coreMap[i] != null){
				System.out.println("coreMap[" + i + "]: ppn = " + coreMap[i].translationEntry.ppn);
				System.out.println("\tvalid = " + coreMap[i].translationEntry.valid + " readOnly = " + coreMap[i].translationEntry.readOnly);
				System.out.println("\tpid = " + coreMap[i].processID + " vpn = " + coreMap[i].translationEntry.vpn);
			}	
		}
	}
	
	private static PageTable instance = null;
	
	/** the formal page table, map from page(<pid, vpn>) to entry */
	private HashMap<ITableItem, TranslationEntry> invertedTable;
	/** In Chinese "fan zhuan ye biao", save pages in memory */
	private WorksetClockItem[] coreMap;
	
	//private static Lock pageTableLock = new Lock();
	
	/** for work set clock algorithm */
	private int clockHand;
	private final long TimeThreshold = 100000;
}
