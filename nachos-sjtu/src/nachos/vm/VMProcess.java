package nachos.vm;

import nachos.machine.*;
import nachos.threads.Lock;
import nachos.userprog.UserProcess;

import java.util.*;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
		saveTLB = new TranslationEntry[Machine.processor().getTLBSize()];
		for(int i = 0; i < saveTLB.length; ++i){
			saveTLB[i] = new TranslationEntry(0, 0, false, false, false, false);
		}
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
		//TLBlock.acquire();
		for(int i = 0; i < saveTLB.length; ++i){
			saveTLB[i] = Machine.processor().readTLBEntry(i);
			if(saveTLB[i].valid){
				PageTable.getInstance().updateEntry(pid, saveTLB[i]);
			}
		}
		//TLBlock.release();
		super.saveState();
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {	
		//TLBlock.acquire();
		for(int i = 0; i < saveTLB.length; ++i) {
            if (saveTLB[i].valid) {
                TranslationEntry Entry = PageTable.getInstance().getPageTableEntry(pid, saveTLB[i].vpn);
                if (Entry != null && Entry.valid){
                    Machine.processor().writeTLBEntry(i, saveTLB[i]);
                }
                else{
                    Machine.processor().writeTLBEntry(i, new TranslationEntry(0, 0, false, false, false, false));
                }
            } 
            else{
                Machine.processor().writeTLBEntry(i, new TranslationEntry(0, 0, false, false, false, false));
            }
        }
		//TLBlock.release();
	}

	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;
				lazySections.put(vpn, new LazyItem(s,i));
			}
		}
		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt> or 
	 * release the pages have been allocated.
	 */
	protected void unloadSections() {
		//allocatedLock.acquire();
		
		Iterator<Integer> iter = allocatedVPN.iterator();
		while (iter.hasNext()) {
			pageLock.acquire();
			Integer vpn = iter.next();
            TranslationEntry entry = PageTable.getInstance().removeEntry(pid, vpn);
            if(entry.valid){
                VMKernel.releasePage(entry);
            }
            SwapFile.getInstance().removeFromDisk(pid, vpn);
            //iter.remove();
            --numPages;
            pageLock.release();
        }
		//allocatedLock.release();
	}
	/*
	protected void releaseAll(){
		
		Iterator<Integer> iter = allocatedVPN.iterator();
		while (iter.hasNext()) {
			Integer vpn = iter.next();
            TranslationEntry entry = PageTable.getInstance().removeEntry(pid, vpn);
            if(entry.valid){
                VMKernel.releasePage(entry);
            }
            SwapFile.getInstance().removeFromDisk(pid, vpn);
            iter.remove();
        }
		
		for(OpenFile ofile: fileTable.values()){
			ofile.close();
		}
		
		for(UserProcess process: childProcesses){
			process.fatherProcess = null;
		}
		
		coff.close();
		
		hasExit = true;
	}
	*/
	
	/**
	 * rewrite the readVirtualMemory
	 * if virtual memory not in page, move it to page, then, write
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		int vpn = vaddr >> offsetLen;
		
		pageLock.acquire();
		
		TranslationEntry Entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
		if(Entry == null){
			Lib.assertNotReached("fuck read");
			acquirePhyPages(vpn, 1, false); //to make sence for dynamic memory allocate 
			Entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
			//pageLock.release();
			//return 0;
			//handleKill(badVirtualAddressException);
		}
		if(!Entry.valid){
			handlePageNotInMemory(Entry.vpn);
		}
		
		Entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
		Lib.assertTrue(Entry != null && Entry.valid);
		
		Entry.used = true;
		PageTable.getInstance().setEntry(pid, Entry);
		
		pageLock.release();
		
		return super.readVirtualMemory(vaddr, data, offset, length);
	}
	
	/**
	 * rewrite writeVirtualMemory in UserProcess
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		int vpn = vaddr >> offsetLen;
		pageLock.acquire();
		
        TranslationEntry entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
        if(entry == null){
        	Lib.assertNotReached("fuck write");
        	acquirePhyPages(vpn, 1, false); //to make sence for dynamic memory allocate
			entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
        	//pageLock.release();
        	//return 0;
			//handleKill(badVirtualAddressException);
		}
		if(!entry.valid){
			handlePageNotInMemory(entry.vpn);
		}
		
		entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
        Lib.assertTrue(entry != null && entry.valid);

        entry.dirty = true;
        entry.used = true;
        PageTable.getInstance().setEntry(pid, entry);

        pageLock.release();
        
        return super.writeVirtualMemory(vaddr, data, offset, length);
    }
	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		switch (cause) {
		case Processor.exceptionTLBMiss:
			handleTLBMiss(Machine.processor().readRegister(Processor.regBadVAddr));
			break;
		default:
			super.handleException(cause);
			break;
		}
	}
	/**
	 * handle TLB miss
	 * 
	 * @param vaddr virtual address
	 * @return -1 if fail or 0 success
	 */
	public int handleTLBMiss(int vaddr) {
		int vpn = vaddr >> offsetLen;
		
		pageLock.acquire();
		
		TranslationEntry Entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
		if(Entry == null){
			pageLock.release();
			handleKill(badVirtualAddressException);
			return -1;
		}
		if(! Entry.valid){
			handlePageNotInMemory(vpn);
			Entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
		}
		int toReplaced = TLBmanager.pickReplaceTLB();
		TLBmanager.replaceTLBEntry(pid, toReplaced, Entry);
		pageLock.release();
		
		return 0;
	}
	
	/**
	 * print memory in physical page number to debug
	 * @param ppn
	 */
	public static void outputMemory(int ppn){
		byte[] memory = Machine.processor().getMemory();
		for(int i = 0; i < 32; ++i){
			for(int j = 0; j < 32; ++j){
				Lib.debug(dbgVM, memory[ppn*pageSize + 32*i + j] + "  ");
			}
			Lib.debug(dbgVM, "");
		}
	}
	
	/**
	 * print TLB to debug
	 */
	public void outputTLB(){
		Lib.debug(dbgVM, "TLB:");
		for(int i = 0; i < Machine.processor().getTLBSize(); ++i){
			TranslationEntry entry = Machine.processor().readTLBEntry(i);
			Lib.debug(dbgVM, "valid vpn ppn " + entry.valid + " " + entry.vpn + " " + entry.ppn + " ");
		}
		Lib.debug(dbgVM, "");
	}
	/**
	 * when a page be need but not in memory, call this function
	 * but can't call when it's already in memory, otherwise it cause
	 * error
	 * 
	 * @param vpn
	 * @return
	 */
	public int handlePageNotInMemory(int vpn){
		
		TranslationEntry entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
		
		if(entry == null){
			Lib.debug(dbgVM, "page " + vpn + "not in PageTable");
			return -1;
		}
		Lib.assertTrue(!entry.valid, "move already in memory");
		
		int ppn = VMKernel.allocatePage();
		if(ppn == -1){ //no free memory page, must be some page to be replaced
			WorksetClockItem replacedPage = PageTable.getInstance().pickPage();
			ppn = replacedPage.translationEntry.ppn;
			movePageToDisk(replacedPage.processID, replacedPage.translationEntry);
			//ugly code for VMProcess to need know PageTable's WorksetClockItem
		}
		movePageToMemory(vpn, ppn);
		Lib.debug(dbgVM, "move page " + pid + " " + vpn + " to memory " + ppn);
		//Lib.debug(dbgVM, "disk pos = " + SwapFile.getInstance().getPos(pid, vpn));
		//PageTable.getInstance().printCoreMap();
		//outputMemory(ppn);
		return 0;
	}
	/**
	 * create or get a free page
	 * @return
	 */
	// temp, never use this
	public int getFreePage(){
		int ppn = VMKernel.allocatePage();
		if(ppn == -1){
			WorksetClockItem replacedPage = PageTable.getInstance().pickPage();
			ppn = replacedPage.translationEntry.ppn;
			movePageToDisk(replacedPage.processID, replacedPage.translationEntry);
			//ugly code for VMProcess to need know PageTable's WorksetClockItem
		}
		return ppn;
	}
	
	/**
	 * rewrite this function to implement VM
	 * 
	 *  @param vpn
	 */
	protected TranslationEntry getTranslationEntry(int vpn){
		return PageTable.getInstance().getPageTableEntry(pid, vpn);
	}
	
	/**
	 * move a Page from memory to disk, and refresh PageTable
	 * 
	 * @param pid
	 * @param entry
	 */
	private void movePageToDisk(int pid, TranslationEntry entry){
		Lib.assertTrue(entry.valid, "page not in memory");
		TLBmanager.flushTLB(pid, entry);
		entry = PageTable.getInstance().getPageTableEntry(pid, entry.vpn);
		
		if(entry.dirty){
			SwapFile.getInstance().PageWriteToFile(pid, entry.vpn, entry.ppn);
		}
		entry.valid = false;
		
		PageTable.getInstance().setEntry(pid, entry);
	}
	
	/**
	 * move a Page from disk to memory, and refresh PageTable
	 * 
	 * @param pid
	 * @param entry
	 */
	private void movePageToMemory(int vpn, int ppn){
		TranslationEntry entry = PageTable.getInstance().getPageTableEntry(pid, vpn);
		Lib.assertTrue(entry != null, "Page not in page table");
        Lib.assertTrue(!entry.valid, "Page is in memory");
        
        boolean dirty;
        if(lazySections.containsKey(vpn)){
        	loadLazySection(vpn, ppn);
        	dirty = true;
        }
        else{
        	SwapFile.getInstance().PageReadFromFile(pid, vpn, ppn);
        	dirty = false;
        }
        
        TranslationEntry newEntry = new TranslationEntry(vpn, ppn, true, entry.readOnly, true, dirty);
        //(int vpn, int ppn, boolean valid, boolean readOnly, boolean used, boolean dirty)
       
        PageTable.getInstance().setEntry(pid, newEntry);
	}
	
	/**
	 * acquire Physical pages in pageTable
	 * 
	 * @param firstVPN
	 * @param needNum
	 * @param readOnly
	 * @return true if success or false if failed
	 */
	protected boolean acquirePhyPages(int firstVPN, int needNum, boolean readOnly){
		
		for(int i = 0; i < needNum; ++i){
	    	PageTable.getInstance().addEntry(pid, new TranslationEntry(firstVPN + i, 0, false, readOnly, false, false));
	        //allocatedLock.acquire();
	    	allocatedVPN.add(firstVPN + i);
	    	//allocatedLock.release();
	    }
	    numPages += needNum;

	    return true;
	}
	

	/**
	 * load the lazy section 
	 * 
	 * @param vpn
	 * @param ppn
	 */
	private void loadLazySection(int vpn, int ppn){
		LazyItem item = lazySections.remove(vpn);
		
		if(item == null){
			return;
		}
		coff.getSection(item.sectionNum).loadPage(item.vpnOffset, ppn);
	}
	
	/**
	 * Item to restore lazySection, reserve two integers: section number and vpnOffset
	 * (vpnOffset + firstVPN in this section = vpn, vpnOffset if the i'th vpn in this section)
	 *
	 * @author Splendid Zheng
	 */
	protected class LazyItem {
		public int sectionNum; 
		public int vpnOffset;
		
		public LazyItem(int s, int v){
			sectionNum = s;
			vpnOffset = v;
		}
		
		public boolean equals(Object o){
			if(o == null || !(o instanceof LazyItem)){
				return false;
			}
			return sectionNum == ((LazyItem)o).sectionNum && vpnOffset == ((LazyItem)o).vpnOffset;
		}
		
		public int hashCode(){
			return (new Integer(sectionNum).toString() + new Integer(vpnOffset).toString()).hashCode();
		}
	}
	/**page size*/
	private static final int pageSize = Processor.pageSize;
	/**Exception for bad Virtual Address*/
	private static final int badVirtualAddressException = 99;
	
	private static final char dbgProcess = 'a';
	private static final char dbgVM = 'v';
	
	private static TLBmanager tlb = new TLBmanager();
	
	/**hash map to store the sections to be lazy load*/
	protected HashMap<Integer, LazyItem> lazySections = new HashMap<Integer, LazyItem>();
	
	/**store the vpn which has been allocated in this process*/
	protected LinkedList<Integer> allocatedVPN = new LinkedList<Integer>();
	
	/**necessary lock to avoid global compaction*/
	protected static Lock pageLock = new Lock();
	
	/** to save and restore TLB in saveState() or restoreState()*/
	protected TranslationEntry[] saveTLB;
	
	/**lock to used when TLB save and restore */
	protected static Lock TLBlock = new Lock();
}
