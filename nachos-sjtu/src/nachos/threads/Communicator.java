package nachos.threads;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		communicatorLock = new Lock();
		speaker = new Condition(communicatorLock);
		listener = new Condition(communicatorLock);
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		communicatorLock.acquire();
		while(hasMessage){
			//listener.wake(); //I omit it but also success
			speaker.sleep(); //when speaker is sleep it release lock, 
		} //but when waked up, it acquire the lock 
		//so we can make sleep in a lock
		messageWord = word;
		hasMessage = true;
		listener.wake();
		
		communicatorLock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		communicatorLock.acquire();
		while(!hasMessage){
			//speaker.wake(); //I omit it but also success
			listener.sleep();//when listener is sleep it release lock, 
		} //but when waked up, it acquire the lock 
		//so we can make sleep in a lock
		
		int message = messageWord;	
		hasMessage = false;
		speaker.wake();
		
		communicatorLock.release();
		return message;
	}
	
	private Lock communicatorLock;
	private Condition speaker;
	private Condition listener;
	
	private int messageWord;
	private boolean hasMessage = false;
}
