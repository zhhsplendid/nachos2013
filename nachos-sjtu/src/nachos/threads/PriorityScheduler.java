package nachos.threads;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Iterator;

import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * A scheduler that chooses threads based on their priorities.
 * 
 * <p>
 * A priority scheduler associates a priority with each thread. The next thread
 * to be dequeued is always a thread with priority no less than any other
 * waiting thread's priority. Like a round-robin scheduler, the thread that is
 * dequeued is, among all the threads of the same (highest) priority, the thread
 * that has been waiting longest.
 * 
 * <p>
 * Essentially, a priority scheduler gives access in a round-robin fassion to
 * all the highest-priority threads, and ignores all other threads. This has the
 * potential to starve a thread if there's always a thread waiting with higher
 * priority.
 * 
 * <p>
 * A priority scheduler must partially solve the priority inversion problem; in
 * particular, priority must be donated through locks, and through joins.
 */
public class PriorityScheduler extends Scheduler {
	/**
	 * Allocate a new priority scheduler.
	 */
	public PriorityScheduler() {
	}

	/**
	 * Allocate a new priority thread queue.
	 * 
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer priority from
	 *            waiting threads to the owning thread.
	 * @return a new priority thread queue.
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new PriorityQueue(transferPriority);
	}

	public int getPriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadState(thread).getPriority();
	}

	public int getEffectivePriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadState(thread).getEffectivePriority();
	}

	public void setPriority(KThread thread, int priority) {
		Lib.assertTrue(Machine.interrupt().disabled());

		Lib.assertTrue(priority >= priorityMinimum
				&& priority <= priorityMaximum);

		getThreadState(thread).setPriority(priority);
	}

	public boolean increasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMaximum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority + 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	public boolean decreasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMinimum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority - 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	/**
	 * The default priority for a new thread. Do not change this value.
	 */
	public static final int priorityDefault = 1;
	/**
	 * The minimum priority that a thread can have. Do not change this value.
	 */
	public static final int priorityMinimum = 0;
	/**
	 * The maximum priority that a thread can have. Do not change this value.
	 */
	public static final int priorityMaximum = 7;

	/**
	 * Return the scheduling state of the specified thread.
	 * 
	 * @param thread
	 *            the thread whose scheduling state to return.
	 * @return the scheduling state of the specified thread.
	 */
	protected ThreadState getThreadState(KThread thread) {
		if (thread.schedulingState == null)
			thread.schedulingState = new ThreadState(thread);

		return (ThreadState) thread.schedulingState;
	}

	/**
	 * A <tt>ThreadQueue</tt> that sorts threads by priority.
	 */
	protected class PriorityQueue extends ThreadQueue {
		PriorityQueue(boolean transferPriority) {
			this.transferPriority = transferPriority;
		}

		public void waitForAccess(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			getThreadState(thread).waitForAccess(this);
		}

		public void acquire(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			getThreadState(thread).acquire(this);
		}

		public KThread nextThread() {
			Lib.assertTrue(Machine.interrupt().disabled());
			// implement me
			
			ThreadState nextThreadState = pickNextThread();
			
			if(nextThreadState == null){
				return null;
			}
			else{
				nextThreadState.acquire(this);
				return nextThreadState.thread;
			}
		}

		/**
		 * Return the next thread that <tt>nextThread()</tt> would return,
		 * without modifying the state of this queue.
		 * 
		 * @return the next thread that <tt>nextThread()</tt> would return.
		 */
		protected ThreadState pickNextThread() {
			// implement me
			ThreadState nextThread = null;
			ThreadState checkThread;
			int priority = priorityMinimum - 1;
			int checkPriority;
			Iterator<ThreadState> iter = waitThreads.iterator();
			while(iter.hasNext()){
				checkThread = iter.next();
				checkPriority = checkThread.getEffectivePriority();
				/* omit these but right on test case...
				if(transferPriority){
					checkPriority = checkThread.getEffectivePriority();
				}
				else{
					checkPriority = checkThread.getPriority();
				}*/
				if(priority < checkPriority){
					priority = checkPriority;
					nextThread = checkThread;
					if(priority == priorityMaximum){
						break;
					}
				}
			}
			
			return nextThread;
		}

		public void print() {
			// implement me (if you want)
			Lib.assertTrue(Machine.interrupt().disabled());
			
			System.out.println("owning threads: " + owningThread);
			System.out.println("transferPriority: " + transferPriority);
			System.out.print("waiting thread:");
			Iterator<ThreadState> iter = waitThreads.iterator();
			while(iter.hasNext()){
				System.out.print(" " + iter.next());
			}
		}

		/**
		 * <tt>true</tt> if this queue should transfer priority from waiting
		 * threads to the owning thread.
		 */
		public boolean transferPriority;
		
		/**
		 * using java.util.LinkedList to save waiting threads
		 */
		protected LinkedList<ThreadState> waitThreads = new LinkedList<ThreadState>();
		
		/**
		 * which thread is owning thread.
		 */
		protected ThreadState owningThread = null;
		
	}

	/**
	 * The scheduling state of a thread. This should include the thread's
	 * priority, its effective priority, any objects it owns, and the queue it's
	 * waiting for, if any.
	 * 
	 * @see nachos.threads.KThread#schedulingState
	 */
	protected class ThreadState {
		/**
		 * Allocate a new <tt>ThreadState</tt> object and associate it with the
		 * specified thread.
		 * 
		 * @param thread
		 *            the thread this state belongs to.
		 */
		public ThreadState(KThread thread) {
			this.thread = thread;

			setPriority(priorityDefault);
		}

		/**
		 * Return the priority of the associated thread.
		 * 
		 * @return the priority of the associated thread.
		 */
		public int getPriority() {
			return priority;
		}

		/**
		 * Return the effective priority of the associated thread.
		 * 
		 * Calculating rule: the owning thread should have highest 
		 * effectivePriority among the waitThreads.
		 * 
		 * The answer is saved in variable effectivePriority, and if answer
		 * hasn't been changed, we can return the saved answer; and each time
		 * the effecitivePriority may change(setPriority(priority) or 
		 * acquire(PriorityQueue waitQueue)) I don't calculate it immediately
		 * calculate when we want to get for efficient.
		 * 
		 * @return the effective priority of the associated thread.
		 */
		public int getEffectivePriority(){
			return getEffectivePriority(new HashSet<KThread>());
		}
		public int getEffectivePriority(HashSet<KThread> visited) {
			// implement me
			
			if(effectivePriority != undefinePriority){
				return effectivePriority;
			}
			if(visited.contains(thread)){
				return effectivePriority;
			}
			visited.add(thread);
			
			effectivePriority = priority;
			Iterator<PriorityQueue> queueIter = owningQueue.iterator();
			Iterator<ThreadState> stateIter;
			PriorityQueue waitQueue;
			int donatePriority;
			
			//System.out.println(this);
			while(queueIter.hasNext()){
				waitQueue = queueIter.next();
				stateIter = waitQueue.waitThreads.iterator();
				
				while(stateIter.hasNext() && waitQueue.transferPriority){
					donatePriority = stateIter.next().getEffectivePriority();
					
					if(donatePriority > effectivePriority){
						
						effectivePriority = donatePriority;
						
						if(effectivePriority == priorityMaximum){
							return effectivePriority; 
						}
					}
				}
			}

			return effectivePriority;
		}

		/**
		 * Set the priority of the associated thread to the specified value.
		 * 
		 * @param priority
		 *            the new priority.
		 */
		public void setPriority(int priority) {
			// implement me
			
			//effective priority may be change after reset priority 
			updateWaiting(this, new HashSet<ThreadState>());
			
			if (this.priority == priority)
				return;

			this.priority = priority;
		}

		/**
		 * Called when <tt>waitForAccess(thread)</tt> (where <tt>thread</tt> is
		 * the associated thread) is invoked on the specified priority queue.
		 * The associated thread is therefore waiting for access to the resource
		 * guarded by <tt>waitQueue</tt>. This method is only called if the
		 * associated thread cannot immediately obtain access.
		 * 
		 * @param waitQueue
		 *            the queue that the associated thread is now waiting on.
		 * 
		 * @see nachos.threads.ThreadQueue#waitForAccess
		 */
		public void waitForAccess(PriorityQueue waitQueue) {
			// implement me
			waitQueue.waitThreads.add(this);
			
			needChangeWaiting.add(waitQueue);
			if(waitQueue.owningThread != null){
				updateWaiting(waitQueue.owningThread, new HashSet<ThreadState>());
			}
		}

		/**
		 * Called when the associated thread has acquired access to whatever is
		 * guarded by <tt>waitQueue</tt>. This can occur either as a result of
		 * <tt>acquire(thread)</tt> being invoked on <tt>waitQueue</tt> (where
		 * <tt>thread</tt> is the associated thread), or as a result of
		 * <tt>nextThread()</tt> being invoked on <tt>waitQueue</tt>.
		 * 
		 * @see nachos.threads.ThreadQueue#acquire
		 * @see nachos.threads.ThreadQueue#nextThread
		 */
		public void acquire(PriorityQueue waitQueue) {
			// implement me
			if(waitQueue.owningThread != null){
				waitQueue.owningThread.owningQueue.remove(waitQueue);
				updateWaiting(waitQueue.owningThread, new HashSet<ThreadState>());
			}
			
			waitQueue.owningThread = this;
			waitQueue.waitThreads.remove(this);
			
			owningQueue.add(waitQueue);
			needChangeWaiting.remove(waitQueue);
			updateWaiting(this, new HashSet<ThreadState>());
		}
		
		/**
		 * called when the thread "now" may called effective priority of "now" waiting thread
		 * changed
		 */
		public void updateWaiting(ThreadState now, HashSet<ThreadState> visited){
			now.effectivePriority = undefinePriority;
			visited.add(now);
			Iterator<PriorityQueue> iter = now.needChangeWaiting.iterator();
			PriorityQueue waitingQueue;
			ThreadState waiting;
			
			while(iter.hasNext()){
				waitingQueue = iter.next();
				waiting = waitingQueue.owningThread;
				if(waitingQueue.transferPriority && waiting != null && !visited.contains(waiting)){
					updateWaiting(waiting, visited);
				}
			}
		}
		/** The thread with which this object is associated. */
		protected KThread thread;
		/** The priority of the associated thread. */
		protected int priority;
		/** The effective priority of the associated thread. */
		protected int effectivePriority;
		
		private final int undefinePriority = priorityMinimum - 1;
		/**
		 * This thread is the owning thread of those priority queue in the hash set
		 */
		protected HashSet<PriorityQueue> owningQueue = new HashSet<PriorityQueue>();
		
		/**
		 * This thread is in some waiting queues, and if queue is transferPriority,
		 * the queue's owing thread need to change effective priority when this thread
		 * changed 
		 */
		protected HashSet<PriorityQueue> needChangeWaiting = new HashSet<PriorityQueue>();
	}
}
