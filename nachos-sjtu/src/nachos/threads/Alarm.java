package nachos.threads;

import java.util.PriorityQueue;

import nachos.machine.*;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {
	/**
	 * Allocate a new Alarm. Set the machine's timer interrupt handler to this
	 * alarm's callback.
	 * 
	 * <p>
	 * <b>Note</b>: Nachos will not function correctly with more than one alarm.
	 */
	public Alarm() {
		Machine.timer().setInterruptHandler(new Runnable() {
			public void run() {
				timerInterrupt();
			}
		});
		
		waitQueue = new PriorityQueue<ThreadAndWakeTime>();
	}

	/**
	 * The timer interrupt handler. This is called by the machine's timer
	 * periodically (approximately every 500 clock ticks). Causes the current
	 * thread to yield, forcing a context switch if there is another thread that
	 * should be run.
	 * 
	 * The thread and wake time in waitQueue, if wake time less then or equals now time, 
	 * let the thread be ready to run.
	 */
	public void timerInterrupt() {
		boolean intStatus = Machine.interrupt().disable();
		long nowTime = Machine.timer().getTime();
		ThreadAndWakeTime waitThread;
		while(waitQueue.size() > 0){
			waitThread = waitQueue.poll();
			if(waitThread.waketime <= nowTime){
				waitThread.thread.ready();
			}
			else{
				waitQueue.add(waitThread);
				break;
			}
		}
		Machine.interrupt().restore(intStatus);
		
		KThread.yield();
	}

	/**
	 * Put the current thread to sleep for at least <i>x</i> ticks, waking it up
	 * in the timer interrupt handler. The thread must be woken up (placed in
	 * the scheduler ready set) during the first timer interrupt where
	 * 
	 * I add the thread called waitUnit(long x) into priority queue.
	 * 
	 * <p>
	 * <blockquote> (current time) >= (WaitUntil called time)+(x) </blockquote>
	 * 
	 * @param x
	 *            the minimum number of clock ticks to wait.
	 * 
	 * @see nachos.machine.Timer#getTime()
	 */
	public void waitUntil(long x) {
		/* for now, cheat just to get something working (busy waiting is bad)
		long wakeTime = Machine.timer().getTime() + x;
		while (wakeTime > Machine.timer().getTime())
			KThread.yield();
		*/
		boolean intStatus = Machine.interrupt().disable();
		long wakeTime = Machine.timer().getTime() + x;
		waitQueue.add(new ThreadAndWakeTime(KThread.currentThread(), wakeTime));
		KThread.sleep();
		Machine.interrupt().restore(intStatus);
	}
	
	/**
	 * Save the thread called waitUnitl(x) and wake time which equals (WaitUntil called time)+(x)
	 */
	private class ThreadAndWakeTime implements Comparable<ThreadAndWakeTime>{
		
		/**
		 * Allocate a new ThreadAndWakeTime
		 * @param threadtowait 
		 * 		the thread called waitUntil(x)
		 * @param time
		 * 		the waketime = (WaitUntil called time)+(x)
		 */
		public ThreadAndWakeTime(KThread threadtowait, long time){
			thread = threadtowait;
			waketime = time;
		}
		private KThread thread;
		private long waketime;
		
		/**
		 * 
		 * compareTo() ranked by waketime
		 * 
		 */
		public int compareTo(ThreadAndWakeTime t) {
			ThreadAndWakeTime thread = t;

			if (waketime < thread.waketime)
				return -1;
			else if (waketime > thread.waketime)
				return 1;
			else
				return 0;
		}
	}
	
	//java's PriorityQueue, not nachos'.
	private PriorityQueue<ThreadAndWakeTime> waitQueue; 
}
