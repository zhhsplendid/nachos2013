package nachos.threads;

import nachos.ag.BoatGrader;
import nachos.machine.Machine;

public class Boat {
	static BoatGrader bg;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();
		
		int adultNumber = 4;
		int childNumber = 4;
		
		System.out.println("\n ***Testing Boats whth " + childNumber + " children, " + adultNumber + " adults***");
		
		begin(adultNumber, childNumber, b);
		
		//System.out.println("\n ***Testing Boats with only 2 children***");
		//begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		//bg = b;

		// Instantiate global variables here

		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.
		/*
		Runnable r = new Runnable() {
			public void run() {
				SampleItinerary();
			}
		};
		KThread t = new KThread(r);
		t.setName("Sample Boat Thread");
		t.fork();
		*/
		bg = b;
		if(children == 0){
			if(adults == 1){
				bg.AdultRowToMolokai();
			}
			if(adults >= 2){
				System.out.println("impossible for all to cross the river");
			}
			return;
		}
		if(children == 1 && adults >= 1){
			System.out.println("impossible for all to cross the river");
			return;
		}
		//follow solve problems about children >= 2, so here needs special condition 
		
		adultAtOahu = adults;
		childAtOahu = children;
		
		pilotBoat = false;
		
		rideBoat = new Semaphore(0);
		boatAtOahu = new Semaphore(1);
		boatAtMolokai = new Semaphore(0);
		childAtOahuLessThen2 = new Semaphore(0);
		
		boatLock = new Lock();
		
		waitRider = new Condition(boatLock);
		lastComeBack = new Condition(boatLock);
		
		
		for(int i = 0; i < adults; ++i){
			KThread adultThread = new KThread(new adultRun());
			adultThread.setName("Adult " + i);
			adultThread.fork();
		}
		
		for(int i = 0; i < children; ++i){
			KThread childThread = new KThread(new childRun());
			childThread.setName("Child " + i);
			childThread.fork();
		}
		
		//We must have these codes to avoid the thread named "main#0" to be end.  
		while(!allArrived){
			KThread.yield();
		}
	}

	static void AdultItinerary() {
		/*
		 * This is where you should put your solutions. Make calls to the
		 * BoatGrader to show that it is synchronized. For example:
		 * bg.AdultRowToMolokai(); indicates that an adult has rowed the boat
		 * across to Molokai
		 */
		//hasChildAtMolokai.P();
		childAtOahuLessThen2.P();	
		//boatAtOahu.P();
		
		boatLock.acquire();
		bg.AdultRowToMolokai();
		boatAtMolokai.V();
		--adultAtOahu;
		
		if(adultAtOahu == 0 && childAtOahu == 0){
			allArrived = true;
		}
		
		boatLock.release();
	}

	static void ChildItinerary() {
		
		boolean atOahu = true;
		boolean lastOne = false;
		while(!allArrived){
			boatLock.acquire();
			if(atOahu){
				if(pilotBoat){
					pilotBoat = false;
					boatLock.release();
					rideBoat.P();
					
					boatLock.acquire();
					
					bg.ChildRideToMolokai();
					atOahu = false;
					--childAtOahu;
					boatAtMolokai.V();
					waitRider.wake();
					
					if(adultAtOahu == 0 && childAtOahu == 0){
						allArrived = true;
					}
				}
				else{
					pilotBoat = true;
					boatLock.release();
					
					boatAtOahu.P();
					
					boatLock.acquire();
					if(childAtOahu == 1){
						if(lastOne){
							lastOne = false;
							lastComeBack.sleep();
						}
						else{
							bg.ChildRowToMolokai();
							boatAtMolokai.V();
							atOahu = false;
							--childAtOahu;
						}
					}
					else{
						rideBoat.V();
						
						bg.ChildRowToMolokai();
						atOahu = false;
						--childAtOahu;
						
						waitRider.sleep();
					}
					if(adultAtOahu == 0 && childAtOahu == 0){
						allArrived = true;
					}
				}
				
			}
			else{
				boatLock.release();
				boatAtMolokai.P();
				
				boatLock.acquire();
				
				if(allArrived){
					boatLock.release();
					return;
				}
				boatAtOahu.V();

				bg.ChildRowToOahu();
				atOahu = true;
				++childAtOahu;
				lastOne = true;
				lastComeBack.wake();
				
				if(childAtOahu < 2){
					childAtOahuLessThen2.V();
				}
			}
			boatLock.release();
		}
	}

	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out
				.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}
	
	//private static Semaphore hasChildAtMolokai;
	private static Semaphore boatAtOahu;
	private static Semaphore boatAtMolokai;
	private static Semaphore rideBoat;
	private static Semaphore childAtOahuLessThen2;
	
	private static Condition waitRider;
	private static Condition lastComeBack;
	
	private static Lock boatLock;
	
	private static boolean pilotBoat; //Is there a child pilot the boat.
	private static boolean allArrived;
	private static int childAtOahu;
	private static int adultAtOahu;
	//private static int childAtMolokai;
	
	
	private static class adultRun implements Runnable{
		adultRun(){}
		
		public void run(){
			AdultItinerary();
		}
	}

	
	private static class childRun implements Runnable{
		childRun() {}	
		
		public void run(){
			ChildItinerary();
		}	
	};
}
